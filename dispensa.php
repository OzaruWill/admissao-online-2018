<?php require_once("header.php"); ?>



<section class="admissao-form-wrapper ">

<section class="container">

	<div class="row">

		<div class="col-xs-12">
			<h1 class="admissao-form-title">DISPENSA DE DISCIPLINAS</h1>
		</div>

	</div><!-- row -->
	<div class="row">

		<div class="col-xs-12">
			<p class="admissao-form-description">Adicione as disciplinas cursadas em outra instituição de Ensino e simule suas dispensas para a matrícula na UNISUAM.</p>
		</div>

	</div><!-- row -->
	<div class="row">

		<div class="col-xs-12">
			<div class="panel panel-default alerta-cinza">
			  <div class="panel-body">
			        <strong>Atenção!</strong> Os campos abaixo são de preenchimento obrigatório. Caso o código e/ou o nome da disciplina não constem na lista fornecida, é possível inserir o nome da disciplina manualmente no respectivo campo e adicioná-la clicando no botão Adicionar. Selecione a Instituição de Ensino. Caso não seja encontrada na lista, clique em avançar e siga com o seu cadastro.
			  </div>
			</div>
		</div>

	</div><!-- row -->
	<div class="row">

		<div class="col-xs-12 col-md-6">

			<div class="input-group admissao-input">
				<label class="admissao-form__label">Instituição de Ensino</label><br />
				<select class="form-control admissao-input">
						  <option disabled="" selected=""></option>
						  <option>2</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
				</select>
			</div>

		</div>

		<div class="col-xs-12 col-md-6">
			
			<div class="input-group admissao-input">
				<label class="admissao-form__label">Nome da Disciplina</label><br />
				<input type="text" class="form-control admissao-input" />
			</div>
			
			<!-- <button type="button" class="btn btn-default admissao-submit-btn">ENTRAR</button> -->
			
		</div>

	</div><!-- row -->

	<div class="row wrap-py">


		<div class="col-xs-12 col-md-12">


			<div class="tb-dispensa">

			<p class="dispensa-titulo">Disciplinas que podem ser compatíveis</p>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr style="border-bottom:solid 1px #ccc; font-weight: bold;">
			    <td width="126">Código da Disciplina</td>
			    <td width="133">Nome da disciplina</td>
			    <td width="163">&nbsp;</td>
			  </tr>
			  <tr>
			    <td>2546786</td>
			    <td>Teorema de Pitágoras</td>
			    <td align="right"><a href="#" class="btn-incluir"><i class="fa fa-plus" aria-hidden="true"></i> Incluir</a></td>
			  </tr>
			  <tr>
			    <td>2546786</td>
			    <td>Teorema de Pitágoras</td>
			    <td align="right"><a href="#" class="btn-incluir"><i class="fa fa-plus" aria-hidden="true"></i> Incluir</a></td>
			  </tr>
			</table>

			</div><!--/ tb-dispensa -->

		</div>


	</div><!-- row -->


	<hr>


	<div class="row">
		
		<div class="col-xs-12 col-md-12">

			<p class="admissao-form-description"><span class="alerta-vermelho">Nehuma disciplina encontrada no sistema!</span> Adicione manualmente utilizando o código ou o nome da disciplina:</p>

		</div><!-- col -->


		
		<div class="col-xs-12 col-md-4">

			<div class="input-group admissao-input">
				<label class="admissao-form__label">Código da Disciplina</label><br />
			  	<input type="password" class="form-control admissao-input" />
			</div>
			
		</div><!-- col -->

		<div class="col-xs-12 col-md-4">

			<div class="input-group admissao-input">
				<label class="admissao-form__label">Nome da Disciplina</label><br />
			  	<input type="password" class="form-control admissao-input" />
			</div>
			
		</div><!-- col -->

		<div class="col-xs-12 col-md-4">

			<div class="input-group mt-btn">
			  	<button class="admissao-submit-btn m-0"><i class="fa fa-plus" aria-hidden="true"></i> INCLUIR</button>
			</div>
			
		</div><!-- col -->

	</div><!-- row -->

<hr>


	<div class="row wrap-py">
		
		<div class="col-xs-12 col-md-12">

			<div class="tb-dispensa">

			<p class="dispensa-titulo">Universidade Estácio de Sá</p>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr style="border-bottom:solid 1px #ccc; font-weight: bold;">
			    <td width="126">Código da Disciplina</td>
			    <td width="133">Nome da disciplina</td>
			    <td width="163">&nbsp;</td>
			  </tr>
			  <tr>
			    <td>2546786</td>
			    <td>Teorema de Pitágoras</td>
			    <td align="right"><a href="#" class="btn-excluir"><i class="fa fa-times" aria-hidden="true"></i> Excluir</a></td>
			  </tr>
			  <tr>
			    <td>2546786</td>
			    <td>Teorema de Pitágoras</td>
			    <td align="right"><a href="#" class="btn-excluir"><i class="fa fa-times" aria-hidden="true"></i> Excluir</a></td>
			  </tr>
			</table>

			</div><!--/ tb-dispensa -->
			
		</div><!-- col -->

	</div><!-- row -->


	<div class="row wrap-py">
		
		<div class="col-xs-12 col-md-12">

			<div class="tb-dispensa">

			<p class="dispensa-titulo">Universidade Veiga de Almeida</p>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr style="border-bottom:solid 1px #ccc; font-weight: bold;">
			    <td width="126">Código da Disciplina</td>
			    <td width="133">Nome da disciplina</td>
			    <td width="163">&nbsp;</td>
			  </tr>
			  <tr>
			    <td>2546786</td>
			    <td>Teorema de Pitágoras</td>
			    <td align="right"><a href="#" class="btn-excluir"><i class="fa fa-times" aria-hidden="true"></i> Excluir</a></td>
			  </tr>
			  <tr>
			    <td>2546786</td>
			    <td>Teorema de Pitágoras</td>
			    <td align="right"><a href="#" class="btn-excluir"><i class="fa fa-times" aria-hidden="true"></i> Excluir</a></td>
			  </tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td><button class="admissao-submit-btn">SOLICITAR DISPENSA</button></td>
			  </tr>
			</table>

			</div><!--/ tb-dispensa -->
			
		</div><!-- col -->

	</div><!-- row -->


	<div class="row">

		<div class="col-xs-12">
			<h1 class="admissao-form-title">RESULTADO DA DISPENSA</h1>
		</div>

	</div><!-- row -->



 	<div class="row">



 		<div class="row wrap-py">
 			<div class="col-xs-12 col-md-12">
 				<div class="tb-dispensa">
 					<p class="dispensa-titulo">Disciplinas identificadas como dispensa pelo sistema</p>
 					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr style="border-bottom:solid 1px #ccc; font-weight: bold;">
					    <td width="126">Código da Disciplina</td>
					    <td width="133">Nome da disciplina</td>
					    <td width="163">&nbsp;</td>
					  </tr>
					  <tr>
					    <td>2546786</td>
					    <td>Teorema de Pitágoras</td>
					    <td align="right"><a href="#" class="btn-excluir"><i class="fa fa-times" aria-hidden="true"></i> Excluir</a></td>
					  </tr>
					  <tr>
					    <td>2546786</td>
					    <td>Teorema de Pitágoras</td>
					    <td align="right"><a href="#" class="btn-excluir"><i class="fa fa-times" aria-hidden="true"></i> Excluir</a></td>
					  </tr>
					</table>
 				</div>
 			</div>
 		</div>


		<div class="row wrap-py">	
			<div class="col-xs-12 col-md-12">
				<div class="tb-dispensa">
				<p class="dispensa-titulo">Disciplinas <span style="text-decoration: underline; font-weight: bold;">não</span> identificadas como dispensa pelo sistema</p>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr style="border-bottom:solid 1px #ccc; font-weight: bold;">
					    <td width="126">Código da Disciplina</td>
					    <td width="133">Nome da disciplina</td>
					    <td width="163">&nbsp;</td>
					  </tr>
					  <tr>
					    <td>2546786</td>
					    <td>Teorema de Pitágoras</td>
					    <td align="right"><a href="#" class="btn-excluir"><i class="fa fa-times" aria-hidden="true"></i> Excluir</a></td>
					  </tr>
					  <tr>
					    <td>2546786</td>
					    <td>Teorema de Pitágoras</td>
					    <td align="right"><a href="#" class="btn-excluir"><i class="fa fa-times" aria-hidden="true"></i> Excluir</a></td>
					  </tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td><button class="admissao-submit-btn">SEGUIR COM A MATRÍCULA</button></td>
					  </tr>
					</table>
				</div><!--/ tb-dispensa -->
			</div><!-- col -->
		</div><!-- row -->






</section><!-- container -->

</section>


<section class="container">

	<section class="admissao-resultado-wrapper admissao-resultado__pagamento">
		<p class="admissao-subtitle admissao-resultado__info"><strong>Sua escolha foi:</strong> <span class="laranja">Administração</span> | <span class="laranja">Bonsucesso</span> | <span class="laranja">Noite</span> | Preço fixo de R$<span class="laranja">660,00</span><small>/mês</small>
		<hr />
		<h1 class="admissao-title">FORMAS DE PAGAMENTO</h1>
		
		<div class="row admissao-pagamento-wrapper">
			<div class="col-xs-12 col-md-4">
				<h2 class="admissao-status__title">Parcelas Disponíveis:</h2>
			</div>
			<div class="col-xs-12 col-md-8 radio-parcelas">
				
					<label class="radio">
					 <input type="radio" name="optionsRadios"  id="parcelas-5x" value="5 vezes" checked>    <label for="parcelas-5x"><em>5x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</em></label>
					</label>
					
					<label class="radio">
					 <input type="radio" name="optionsRadios"  id="parcelas-6x" value="6 vezez">    <label for="parcelas-6x"><em>6x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</em></label>
					</label>
					
					<label class="radio">
					 <input type="radio" name="optionsRadios"  id="parcelas-7x" value="7 vezes">    <label for="parcelas-7x"><em>7x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</em></label>
					</label>
					
					<label class="radio">
					 <input type="radio" name="optionsRadios"  id="parcelas-8x" value="8 vezes">    <label for="parcelas-8x"><em>8x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</em></label>
					</label>
					
			</div>
		</div>	
			
			<div class="row">
				<div class="col-xs-12 admissao-contrato-wrapper">
					<h1 class="admissao-title">CONTRATO DE MATRÍCULA</h1>
					<div class="alert alert-warning alert__contrato">
						<i class="fa fa-warning"></i>
						<strong>Leia o contrato</strong> a seguir com atenção. Após, preencha os campos no final do texto para assinar virtualmente.
						<br>
					</div>
					
					<iframe class="iframe__contrato" src="contrato-2018-2.content.php"></iframe>
					<hr />
					<h1 class="admissao-title">ACEITE DO CONTRATO DE MATRÍCULA</h1>
				</div>
				
				<div class="col-xs-12 col-md-4 col-md-offset-4 admissao-assinatura-wrapper">
				
					<div class="input-group admissao-input">
						<label class="admissao-form__label">CPF:</label><br />
						<input type="text" class="form-control " placeholder="CPF" />
					</div>
					<div class="input-group admissao-input">
						<label class="admissao-form__label">Senha:</label><br />
						<input type="password" class="form-control " placeholder="Senha" />
					</div>
					<button class="admissao-submit-btn">ASSINAR</button>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs12 admissao-contrato-wrapper">
					<p class="assinatura-texto__p">Contrato assinado por <strong>Maurício Gonçalves Fernandes Pacheco</strong> no dia <strong>00/00/0000</strong> às <strong>00h00</strong>.</p>
					<br />
					<button type="button" class="btn-imprimir-resultado center-block"><i class="fa fa-print"></i>  IMPRIMIR CONTRATO</button>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 admissao-conclusao-wrapper">
					<h1 class="admissao-title">MAIS UMA FASE CONCLUÍDA!</h1>
					<p class="conclusao-texto__p"><i class="fa fa-smile-o fa-3x"></i></p>
					<p class="conclusao-texto__p"><strong class="laranja">Parabéns!</strong> Você concluiu a sua inscrição.</p>
					<p class="conclusao-texto__p">Agora você está muito perto de #SERUNISUAM. Você será <strong>direcionado para seu Ambiente do Aluno</strong>, onde poderá <strong>enviar os documentos</strong> para concluir a sua matrícula.</p>
					<p class="conclusao-texto__p"><em>Ah! Vamos te enviar um e-mail com seu número de matrícula.</em></p>
					<button class="admissao-submit-btn">IR PARA O AMBIENTE</button>
				</div>
			</div>
			
		
	</section>

</section>



<?php require_once("footer.php"); ?>