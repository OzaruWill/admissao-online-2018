<!DOCTYPE html>
<html lang="br">

<head>
    <meta charset="utf-8" />
    <title>Processo de Admissão por Vestibular</title>

    <meta name="description" content="and Validation" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- Cole esse código o mais alto possível na tag HEAD da página: -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5CDT7BP');</script>
    <!-- End Google Tag Manager -->
    <!-- Essa deve ser inserida dentro da tag HEAD -->

    <!-- basic styles -->
    <link href="http://admissao.unisuam.edu.br/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://admissao.unisuam.edu.br/assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://admissao.unisuam.edu.br/unisuam/font-awesome-4.6.3/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://admissao.unisuam.edu.br/assets/css/ace-fonts.css" />
    <link rel="stylesheet" href="http://admissao.unisuam.edu.br/assets/css/ace.min.css" />
    <link rel="stylesheet" href="http://admissao.unisuam.edu.br/assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="http://admissao.unisuam.edu.br/assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="http://admissao.unisuam.edu.br/unisuam/css/style.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
    <![endif]-->
    <script src="http://admissao.unisuam.edu.br/assets/js/ace-extra.min.js"></script>

    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    
</head>

<body class="fundo">

<!-- Além disso, cole esse código imediatamente após a tag de abertura <body> -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5CDT7BP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

		<div class="topbar">
			<div class="navbar-header pull-left simbolo-unisuam">
				<small><img src="http://admissao.unisuam.edu.br/unisuam/images/unisuam-coruja.png" /></small>				
			</div>
		
            <div class="navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">
                    	
                    <li class="transparent">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <span>
                                <i class="fa fa-user icon-animated-hand-pointer" aria-hidden="true"></i> <small>Bem-vindo,</small> Mauricio
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </a>
    
                        <ul class="user-menu dropdown-blue pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                            <li class="dropdown-header">
                                Mauricio Gonçalves Fernandes Pacheco
                            </li>
                            <li class="grey dados-topo">CPF: <strong>000.000.000-00</strong></li>
							<li class="grey dados-topo">Curso: <strong>Administração</strong></li>
							<li class="grey dados-topo">Unidade: <strong>Bonsucesso</strong></li>
							<li class="grey dados-topo">Turno: <strong>Noite</strong></li>
                            <li class="grey dados-topo"><small>Inscrição por</small> <strong>Vestibular</strong></li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-edit"></i>
                                    Editar dados
                                </a>
                            </li>
    
                            <li>
                                <a href="#">
                                    <i class="fa fa-power-off red"></i>
                                    <span class="red no-hover-underline">Sair</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
		</div>

<div class="page-content fundo">
    <div class="row">
        <div class="col-xs-12">

            <div class="form-container">
                <div class="topo">
                    <h1 class="ajuste-logo center">
                        <img src="http://admissao.unisuam.edu.br/unisuam/images/logo-unisuam-topo.png" />
                    </h1>
                </div>
				
                <!-------------------------------------- LOGIN ----------------------------------->
                <div class="position-relative">
                    <div id="login-box" class="login-box visible no-border">
                        <div class="widget-body">
                            <div class="widget-main pd-contrato">
                                <div class="page-header text-center">
                                    <h1>
                                        Inscrição por <strong>VESTIBULAR</strong>
                                    </h1>
                                </div><!-- /.page-header -->
                                <!------------------------------------------------- FORMULÁRIO --------------------------------------------->




                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="btn-toolbar">
												<div class="btn-group">
													<a href="vestibular-curso-edicao.html" class="btn btn-light btn-20"><i class="fa fa-book" aria-hidden="true"></i><br /><span class="hide-mobile">CURSO</span></a>
													<a href="vestibular-resultado.html" class="btn btn-light btn-20"><i class="fa fa-certificate" aria-hidden="true"></i><br /><span class="hide-mobile">RESULTADO</span></a>
													<a href="vestibular-pagamento.html" class="btn btn-light btn-20"><i class="fa fa-credit-card" aria-hidden="true"></i><br /><span class="hide-mobile">PAGAMENTO</span></a>
													<a href="vestibular-documento.html" class="btn btn-ativo btn-20"><i class="fa fa-file-text-o" aria-hidden="true"></i><br /><span class="hide-mobile">DOCUMENTOS</span></a>
													<a href="vestibular-confirmacao.html" class="btn btn-light btn-20 disabled"><i class="fa fa-graduation-cap" aria-hidden="true"></i><br /><span class="hide-mobile">CONFIRMAÇÃO</span></a>
												</div>
											</div>

                                            <div class="step-content row-fluid position-relative" id="step-container">
                                                
                                                <div class="step-pane active" id="step1">
                                                       <h3 class="smaller center">Acompanhamento de documentação</h3><br />
													   
													   <form class="form-horizontal" method="get">
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right">CPF:</label>
				
																<div class="col-xs-12 col-sm-9">
																	<span class="input-icon">
																	<input type="text" id="form-field-mask-1" name="" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 input-mask-cpf" placeholder="000.000.000-00" />
																	<i class="ace-icon small-icon fa fa-asterisk"></i>
																	</span>
																</div>
															</div>
															
															
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right">RG:</label>
				
																<div class="col-xs-12 col-sm-9">
																	<span class="input-icon">
																	<input type="text" id="" name="" class="col-xs-12 col-sm-12 col-md-9 col-lg-9" />
																	<i class="ace-icon small-icon fa fa-asterisk"></i>
																	</span>
																</div>
															</div>
															
															<br />
															<div class="form-group">
                                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">Escola onde concluiu o Ensino Médio:</label>
            
                                                            <div class="col-xs-12 col-sm-9">
                                                                <span class="input-icon">
                                                                <input type="text" id="" name="" class="col-xs-12 col-sm-8 col-md-9 col-lg-9" />
                                                                <i class="ace-icon small-icon fa fa-asterisk"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">Ano de conclusão:</label>
            
                                                            <div class="col-xs-12 col-sm-9">
                                                                <input id="" class="col-xs-12 col-sm-12 col-md-9 col-lg-9" type="text">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">Estado de conclusão:</label>
            
                                                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                                <select id="" class="col-xs-12 col-sm-9 col-md-9 col-lg-9" type="select" required>
                                                                  <option>Selecione</option>
                                                                  <option>AM</option>
                                                                  <option>AP</option>
                                                                  <option>RJ</option>
                                                                  <option>SC</option>
                                                                  <option>SP</option>
                                                              </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                              <label class="control-label col-xs-12 col-sm-3 no-padding-right">Tipo de Escola:</label>
              
                                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                                  <label class="pull-left margin-label">
                                                                      <input name="escola" type="radio" class="ace" />
                                                                      <span class="lbl"> Pública</span>
                                                                  </label>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="escola" type="radio" class="ace" />
                                                                      <span class="lbl"> Particular</span>
                                                                  </label>
                                                              </div>
                                                        </div>
															
														</form>
														
														<br />
														
														<h3>Envio de documentos</h3><hr />
														<div class="table-responsive">
															<table id="sample-table-1" class="table table-striped table-hover space-top">
																<thead>
																	<tr>
																		<th>Documento</th>
																		<th>Parecer do atendente</th>
																		<th>Upload</th>
																		<th>Status</th>
																	</tr>
																</thead>

																<tbody>
																	<tr>
																		<td>CPF</td>
																		<td></td>
																		<td><button class="btn btn-xs btn-inverse" data-toggle="modal"  href="#upload-doc"><i class="fa fa-folder-open bigger-120" aria-hidden="true"></i></button></td>
																		<td><span class="label label-sm label-warning">Em análise</span></td>
																	</tr>
																	
																	<tr>
																		<td>RG</td>
																		<td></td>
																		<td><button class="btn btn-xs btn-inverse" data-toggle="modal"  href="#upload-doc"><i class="fa fa-folder-open bigger-120" aria-hidden="true"></i></button></td>
																		<td><span class="label label-sm label-success">Aprovado</span></td>
																	</tr>
																	
																	<tr>
																		<td>Diploma do Ensino Médio</td>
																		<td>Imagem enviada está embaçada. É preciso enviar novas imagens com boa legibilidade.</td>
																		<td><button class="btn btn-xs btn-inverse" data-toggle="modal"  href="#upload-doc"><i class="fa fa-folder-open bigger-120" aria-hidden="true"></i></button></td>
																		<td><span class="label label-sm label-danger">Reprovado</span></td>
																	</tr>
																	
																	<tr>
																		<td>Histórico do Ensino Médio</td>
																		<td></td>
																		<td><button class="btn btn-xs btn-inverse" data-toggle="modal"  href="#upload-doc"><i class="fa fa-folder-open bigger-120" aria-hidden="true"></i></button></td>
																		<td><span class="label label-sm label-inverse">Não enviado</span></td>
																	</tr>
																</tbody>
															</table>
														</div><!-- /.table-responsive -->
														
														
														<br /><hr />
														
														<div class="col-xs-12 text-center space-top space-bottom-35">
															<strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ATENÇÃO:</strong> Será preciso declarar mais documentos ao longo do seu primeiro período de estudos para que você possa se formar sem problemas, mas se você preferir pode efetuar o <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#nao-tenho">cadastro agora</a>.
															</div>
															
														
															<!--------------------- ACORDION PARA MAIS DOCUMENTOS --------------------->	
																<div id="nao-tenho" class="panel-collapse collapse" style="height: auto;">
																  <div class="panel-body center">
																	<div class="form-group">
																		<div class="form-group">
																			<label class="control-label col-xs-12 col-sm-3 no-padding-right">CPF:</label>
							
																			<div class="col-xs-12 col-sm-9">
																				<span class="input-icon">
																				<input type="text" id="form-field-mask-1" name="" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 input-mask-cpf" />
																				<i class="ace-icon small-icon fa fa-asterisk"></i>
																				</span>
																			</div>
																		</div>
																		
																		
																		<div class="form-group">
																			<label class="control-label col-xs-12 col-sm-3 no-padding-right">RG:</label>
							
																			<div class="col-xs-12 col-sm-9">
																				<span class="input-icon">
																				<input type="text" id="" name="" class="col-xs-12 col-sm-12 col-md-9 col-lg-9" />
																				<i class="ace-icon small-icon fa fa-asterisk"></i>
																				</span>
																			</div>
																		</div>
																		
																		
																		<div class="form-group">
																			<label class="control-label col-xs-12 col-sm-3 no-padding-right">Diploma do Ensino Médio:</label>
							
																			<div class="col-xs-12 col-sm-9">
																				<span class="input-icon">
																				<input type="text" id="" name="" class="col-xs-12 col-sm-12 col-md-9 col-lg-9" />
																				<i class="ace-icon small-icon fa fa-asterisk"></i>
																				</span>
																			</div>
																		</div>
																		
																		
																		<div class="form-group">
																			<label class="control-label col-xs-12 col-sm-3 no-padding-right">Histórico do Ensino Médio:</label>
							
																			<div class="col-xs-12 col-sm-9">
																				<span class="input-icon">
																				<input type="text" name="" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 input-mask-cell" />
																				<i class="ace-icon small-icon fa fa-asterisk"></i>
																				</span>
																			</div>
																		</div>
																	</div>
																  </div>
																</div>
																<!--------------------- ACORDION PARA MAIS DOCUMENTOS --------------------->
													    
                                                         
                                                </div>
                                                  
                                            </div>

                                            
                                        </div><!-- /widget-main -->
                                    </div><!-- /widget-body -->


                                <!------------------------------------------------- FINAL - INSTITUICAO --------------------------------------------->
                            </div><!-- /widget-main -->
                        </div><!-- /widget-body -->
                    </div><!-- /forgot-box -->


                    <div class="row">
                        <div class="col-xs-12 login-info">
                            <hr />
                            <p><strong>Central de informações:</strong><br /> 21 3882-9797 | 21 3882-9752<br />
                                © 2018 UNISUAM. Todos os direitos reservados.</p>
                        </div>
                    </div>
					
					<!-----------------MODAL---------------------->
					<div id="upload-doc" class="modal" tabindex="-1">
					  <div class="modal-dialog modal-mobile">
						  <div class="modal-content">
							  <div class="modal-header modal-header-mobile">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4 class="blue bigger">CPF</h4>
							  </div>

								<div class="modal-body overflow-visible">
									<div class="row margin-mobile-none">
										<div class="col-xs-12 col-sm-12 margin-mobile-none">
											
											<h5>Clique abaixo para procurar o arquivo ou arraste-o para o trecho destacado.</h5>
											
											<div class="widget-body">
												<div class="widget-main">
													<input multiple="" type="file" id="id-input-file-3" />														
												</div>
											</div>
											
											<br />
											
											<h4>Arquivos enviados</h4>
											<table id="sample-table-1" class="table table-striped table-hover table-responsive space-top">
												<thead>
													<tr>
														<th>Nome do arquivo</th>
														<th>Data de envio</th>
														<th>Excluir</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>cpf-mauricio-pacheco.pdf</td>
														<td>00/00/0000 às 00h00 </td>
														<td><button class="btn btn-xs btn-danger" data-toggle="modal"  href="#upload-doc"><i class="fa fa-trash bigger-120" aria-hidden="true"></i></button></td>
													</tr>
												</tbody>
											</table>
									  
										</div>
									</div>
								</div>

							  <div class="modal-footer modal-footer-mobile">
								  <button type="submit" class="btn btn-sm btn-primary">
									  <i class="fa fa-check"></i> 
									  Enviar para análise
								  </button>
								  
								  <button type="submit" class="btn btn-sm btn-danger" data-dismiss="modal">
									  <i class="fa fa-times"></i> 
									  Fechar sem enviar
								  </button>  
							  
							  </div>
						  </div>
					  </div>
				  </div><!-- MODAL -->

                </div><!-- /position-relative -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.main-container -->




<!-- basic scripts -->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>
<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="http://admissao.unisuam.edu.br/assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="http://admissao.unisuam.edu.br/assets/js/fuelux/fuelux.wizard.min.js"></script>
<!-- ace scripts -->
<script src="http://admissao.unisuam.edu.br/assets/js/ace-elements.min.js"></script>
<script src="http://admissao.unisuam.edu.br/assets/js/ace.min.js"></script>
<!-- inline scripts related to this page -->
<script src="http://admissao.unisuam.edu.br/assets/js/jquery.inputlimiter.1.3.1.min.js"></script>

<script src="http://admissao.unisuam.edu.br/assets/js/jquery.maskedinput.min.js"></script>

<script type="text/javascript">
    jQuery(function($) {
        var $validation = false;
        $('#passo-a-passo').ace_wizard().on('change' , function(e, info){
            if(info.step == 1 && $validation) {
                if(!$('#validation-form').valid()) return false;
            }
        }).on('finished', function(e) {
            bootbox.dialog({
                message: "Thank you! Your information was successfully saved!",
                buttons: {
                    "success" : {
                        "label" : "OK",
                        "className" : "btn-sm btn-primary"
                    }
                }
            });
        }).on('stepclick', function(e){
            //return false;//prevent clicking on steps
        });
    });
	
	$.mask.definitions['~']='[+-]';
				$('.input-mask-date').mask('99/99/9999');
				$('.input-mask-cell').mask('(99) 99999-9999');
				$('.input-mask-tel').mask('(99) 9999-9999');
				$('.input-mask-cep').mask('99.999-999');
				$('.input-mask-cpf').mask('999.999.999-99');
				$('.input-mask-rg').mask('99.999.999-9');
				$('.input-mask-eleitor').mask('9999.9999.9999');
				$('.input-mask-zona').mask('999');
				$('.input-mask-secao').mask('9999');
	
				
				$('#id-input-file-3').ace_file_input({
					style:'well',
					btn_choose:'Clique ou arraste o arquivo aqui para enviar',
					btn_change:null,
					no_icon:'fa-cloud-upload',
					droppable:true,
					thumbnail:'small'//large | fit
					//,icon_remove:null//set null, to hide remove/reset button
					/**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
					/**,before_remove : function() {
						return true;
					}*/
					,
					preview_error : function(filename, error_code) {
						//name of the file that failed
						//error_code values
						//1 = 'FILE_LOAD_FAILED',
						//2 = 'IMAGE_LOAD_FAILED',
						//3 = 'THUMBNAIL_FAILED'
						//alert(error_code);
					}
			
				}).on('change', function(){
					//console.log($(this).data('ace_input_files'));
					//console.log($(this).data('ace_input_method'));
				});
				
			
				
</script>
</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-wizard.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 11 Apr 2014 00:52:08 GMT -->
</html>