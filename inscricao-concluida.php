<?php require_once("header.php"); ?>

<section class="container">
	<section class="admissao-form-wrapper">

		<div class="row" style="margin-bottom: 30px;">

			<div class="col-xs-12" style="margin-bottom: 20px;">

				<h1 class="admissao-form-title"><i class="fa fa-thumbs-up" aria-hidden="true"></i> INSCRIÇÃO CONCLUÍDA COM SUCESSO</h1>

			</div><!-- col -->


			<div class="col-md-12">

				<div class="input-group admissao-input" style="margin-bottom: 30px;">
					<button type="button" class="btn-imprimir-resultado center-block"><i class="fa fa-print"></i>  IMPRIMIR O COMPROVANTE</button>
				</div>

				<div class="input-group admissao-input">
					<div class="info-inscricao-concluida">	
						Confira os dados de inscrição abaixo e compareça no dia agendado para realizar a prova e <strong>conquistar sua vaga</strong> na Graduação UNISUAM. Lembre-se que no dia da prova é preciso apresentar-se com o <strong>comprovante de inscrição</strong>, sua <strong>carteira de identidade original</strong> e <strong>caneta azul ou preta</strong>.
					</div>
				</div>

			</div><!-- col -->
			
		</div><!-- row -->

		<div class="row">

			<div class="col-md-6 text-center">

					<i class="fa fa-thumb-tack fa-3x" style="margin: 0 auto 15px auto; color: #ee8325;" aria-hidden="true"></i>

					<h2 class="concluida__title">Confira abaixo o local e horário para a realização da prova:</h2>
					
					<p class="texto-ic"><strong>Local da prova:</strong> Unidade UNISUAM-RJ - Campo Grande<br>
					<strong>Endereço:</strong> Av.Cesário de Melo, 2571 - Campo Grande - Rio de Janeiro<br>
					<strong>Data:</strong> 18/05/2018<br>
					<strong>Horário:</strong> 13:00<br>
					<span style="font-size: 2.8rem;"><strong>Sala:</strong> 007CG1</span></p>

			</div><!-- col -->

			<div class="col-md-6 text-center">

				<i class="fa fa-user-circle-o fa-3x" style="margin: 0 auto 15px auto;  color: #ee8325;" aria-hidden="true"></i>

					<h2 class="concluida__title">Dados pessoais do candidato do Vest UNISUAM:</h2>
					
					<p class="texto-ic">
					<strong>Nome:</strong> Isaquiel Senna<br>
					<strong>CPF:</strong> 42002727767<br>
					<strong>1ª Opção de Curso:</strong> Administração (Manhã) - UNISUAM-RJ - Bangu<br>

			</div><!-- col -->

		</div><!-- row -->


		<div class="row">

			<div class="col-md-12">

				<div class="input-group admissao-input">
					<h1 class="admissao-form-title">O QUE LEVAR?</h1>
				</div>

			</div><!-- col -->

			<div class="col-md-4">

				<div class="input-group admissao-input">
					
					<div class="box-oqlevar">
						<i class="fa fa-file-text-o" aria-hidden="true"></i> Comprovante de Inscrição
					</div>

				</div>

			</div><!-- col -->

			<div class="col-md-4">

				<div class="input-group admissao-input">
					
					<div class="box-oqlevar">
						<i class="fa fa-user" aria-hidden="true"></i> Carteira de Identidade original
					</div>

				</div>

			</div><!-- col -->

			<div class="col-md-4">

				<div class="input-group admissao-input">
					
					<div class="box-oqlevar">
						<i class="fa fa-pencil" aria-hidden="true"></i> Caneta azul ou preta
					</div>

				</div>

			</div><!-- col -->

		</div><!-- row -->

	</section>
</section>


<?php require_once("footer.php"); ?>