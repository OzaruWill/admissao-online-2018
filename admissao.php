<?php require_once("header.php"); ?>

<section class="container">

	<section class="admissao-form-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="admissao-form-title">ESCOLHA SUA FORMA DE INGRESSO</h1>
			</div>
			
	<!--================================================================= ENEM =======================================================-->        
			
			<div class="col-xs-12 col-md-3 forma-de-ingresso-wrapper__enem">
				<div class="forma-de-ingresso__box forma-de-ingresso__enem">
					<i class="fa fa-2x fa-star"></i>
					<p class="forma-de-ingresso__label">ENEM</p>
					<hr class="forma-de-ingresso-box__divider" />
					<p class="forma-de-ingresso__desc">Use sua nota do ENEM.</p>
				</div>
				<button class="documentacao-box documentacao-box__enem" type="button" data-toggle="modal" data-target="#modal-documentacao__enem">
					<i class="fa fa-file-text"></i> Documentação necessária
				</button>
			</div>

			<!-- Modal Documentação ENEM -->
			<div class="modal fade" id="modal-documentacao__enem" tabindex="-1" role="dialog" aria-labelledby="modal-documentacao__enemTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered" role="document">
				
				<div class="modal-content">
					<button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
					  <i class="fa fa-times" aria-hidden="true"></i>
					</button>      
					  <div class="modal-body">
						<h4>Documentos necessários para matrícula via ENEM</h4>
						  <ul>
							<li>Certificado de conclusão do Ensino Médio, com publicação no Diário Oficial;</li>
							<li>Histórico Escolar do Ensino Médio;</li>
							<li>Certidão de Nascimento ou Casamento;</li>
							<li>Carteira de identidade;</li>
							<li>Título de Eleitor;</li>
							<li>Certificado de Reservista ou equivalente;</li>
							<li>Cadastro de Pessoa Física (CPF/o documento);</li> 
							<li>Comprovante de residência;</li>   
						  </ul>
					  </div>
				</div>
			  </div>

			</div>
			
	<!--================================================ VEST ================================================-->        
					
			<div class="col-xs-12 col-md-3 forma-de-ingresso-wrapper__vest">
				<div class="forma-de-ingresso__box forma-de-ingresso__vest">
					<i class="fa fa-2x fa-pencil"></i>
					<p class="forma-de-ingresso__label">VEST</p>
					<hr class="forma-de-ingresso-box__divider" />
					<p class="forma-de-ingresso__desc">Quero fazer a prova.</p>
				</div>
				<button class="documentacao-box documentacao-box__vest" type="button" data-toggle="modal" data-target="#modal-documentacao__vest">
					<i class="fa fa-file-text"></i> Documentação necessária
				</button>
			</div>
			
			<!-- Modal Documentação VEST -->
			<div class="modal fade" id="modal-documentacao__vest" tabindex="-1" role="dialog" aria-labelledby="modal-documentacao__vestTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered" role="document">
				
				<div class="modal-content">
					<button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
					  <i class="fa fa-times" aria-hidden="true"></i>
					</button>      
					  <div class="modal-body">
						<h4>Documentos necessários para matrícula via VEST</h4>
						  <ul>
							<li>Certificado de conclusão do Ensino Médio, com publicação no Diário Oficial;</li>
							<li>Histórico Escolar do Ensino Médio;</li>
							<li>Certidão de Nascimento ou Casamento;</li>
							<li>Carteira de identidade;</li>
							<li>Título de Eleitor;</li>
							<li>Certificado de Reservista ou equivalente;</li>
							<li>Cadastro de Pessoa Física (CPF/o documento);</li> 
							<li>Comprovante de residência;</li>   
						  </ul>
					  </div>
				</div>
			  </div>
			</div>
			
			
			
	<!--============================================ PORTADOR DE DIPLOMA ===========================================-->        
			
			<div class="col-xs-12 col-md-3 forma-de-ingresso-wrapper__portador">
				<div class="forma-de-ingresso__box forma-de-ingresso__portador">
					<i class="fa fa-2x fa fa-graduation-cap"></i>
					<p class="forma-de-ingresso__label">PORTADOR DE DIPLOMA</p>
					<hr class="forma-de-ingresso-box__divider" />
					<p class="forma-de-ingresso__desc">Minha segunda graduação.</p>
				</div>
				<button class="documentacao-box documentacao-box__portador" type="button" data-toggle="modal" data-target="#modal-documentacao__portador">
					<i class="fa fa-file-text"></i> Documentação necessária
				</button>
			</div>
			
			<!-- Modal Documentação ENEM -->
			<div class="modal fade" id="modal-documentacao__portador" tabindex="-1" role="dialog" aria-labelledby="modal-documentacao__portadorTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered" role="document">
				
				<div class="modal-content">
					<button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
					  <i class="fa fa-times" aria-hidden="true"></i>
					</button>      
					  <div class="modal-body">
						<h4>Documentos necessários para matrícula de Portadores de Diploma</h4>
						  <ul>
							<li>Certificado de conclusão do Ensino Médio, com publicação no Diário Oficial;</li>
							<li>Histórico Escolar do Ensino Médio;</li>
							<li>Certidão de Nascimento ou Casamento;</li>
							<li>Carteira de identidade;</li>
							<li>Título de Eleitor;</li>
							<li>Certificado de Reservista ou equivalente;</li>
							<li>Cadastro de Pessoa Física (CPF/o documento);</li> 
							<li>Comprovante de residência;</li>   
						  </ul>
					  </div>
				</div>
			  </div>
			</div>
			

	<!--============================== TRANSFERÊNCIA ==============================-->      

			
			<div class="col-xs-12 col-md-3 forma-de-ingresso-wrapper__transferencia">
				<div class="forma-de-ingresso__box forma-de-ingresso__transferencia">
					<i class="fa fa-2x fa fa-exchange"></i>
					<p class="forma-de-ingresso__label">TRANSFERÊNCIA</p>
					<hr class="forma-de-ingresso-box__divider" />
					<p class="forma-de-ingresso__desc">Vou me transferir.</p>
				</div>
				<button class="documentacao-box documentacao-box__transferencia" type="button" data-toggle="modal" data-target="#modal-documentacao__transferencia">
					<i class="fa fa-file-text"></i> Documentação necessária
				</button>
			</div>
			
			<!-- Modal Documentação TRANSFERÊNCIAS -->
			<div class="modal fade" id="modal-documentacao__transferencia" tabindex="-1" role="dialog" aria-labelledby="modal-documentacao__transferenciaTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered" role="document">
				
				<div class="modal-content">
					<button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
					  <i class="fa fa-times" aria-hidden="true"></i>
					</button>      
					  <div class="modal-body">
						<h4>Documentos necessários para matrícula via TRANSFERÊNCIA</h4>
						  <ul>
							<li>Certificado de conclusão do Ensino Médio, com publicação no Diário Oficial;</li>
							<li>Histórico Escolar do Ensino Médio;</li>
							<li>Certidão de Nascimento ou Casamento;</li>
							<li>Carteira de identidade;</li>
							<li>Título de Eleitor;</li>
							<li>Certificado de Reservista ou equivalente;</li>
							<li>Cadastro de Pessoa Física (CPF/o documento);</li> 
							<li>Comprovante de residência;</li>   
						  </ul>
					  </div>
				</div>
			  </div>
			</div>
		</div>	
		

		<section class="admissao-form-wrapper" style="margin-top:10px;">
			<div class="row">
				<div class="col-xs-12 text-center">
					<div class="btn-vest-solidario">	
						Se pretende realizar o <strong>Vestibular Solidário</strong> e estiver dentro do perfil, <a href="#" class="link-solidario">clique aqui</a>.
					</div>
				</div>
		</section>


		<hr class="forma-de-ingresso__divider divider__vest" />
	</section>


	<!--============================== INSCRIÇÃO ==============================-->  


	<section class="admissao-form-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="admissao-form-title">INSCREVA-SE</h1>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Nome:</label><br />
					<input type="text" class="form-control admissao-input" placeholder="Nome" />
				 </div>
				 <div class="input-group admissao-input">
					<label class="admissao-form__label">Sobrenome:</label><br />
				  <input type="text" class="form-control admissao-input" placeholder="Sobrenome" />
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">E-mail:</label><br />
					<input type="text" class="form-control admissao-input" placeholder="E-mail">
				</div>
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Telefone:</label><br />
					<input type="text" class="form-control admissao-input" placeholder="Telefone">
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<label class="admissao-form__label">Captcha:</label><br />
				<button type="button" class="btn btn-default admissao-submit-btn">ENVIAR</button>
			</div>
		</div>
		<hr/>
	</section>





	<!--=============================================================

					OPÇÃO CLICADA: VEST

	-=============================================================-->
	<section class="admissao-form-wrapper">

		<div class="row">
			<div class="col-xs-12 col-md-3">
				<div class="admissao-input">
					<label class="admissao-form__label">Unidade de realização:</label><br />
				
					<select class="form-control admissao-input">
					  <option disabled selected>Unidade de realização</option>
					  <option>2</option>
					  <option>3</option>
					  <option>4</option>
					  <option>5</option>
					</select>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-3">
				<div class="admissao-input">
					<label class="admissao-form__label">Data da prova:</label><br />
				
					<select class="form-control admissao-input">
					  <option disabled selected>Data da prova</option>
					  <option>2</option>
					  <option>3</option>
					  <option>4</option>
					  <option>5</option>
					</select>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-3">
				<div class="admissao-input">
					<label class="admissao-form__label ">Hora da prova:</label><br />

					<select class="form-control admissao-input">
					  <option disabled selected>Hora da prova</option>
					  <option>2</option>
					  <option>3</option>
					  <option>4</option>
					  <option>5</option>
					</select>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-3">
				<div class="admissao-input">
					<label class="admissao-form__label">Língua estrangeira:</label><br />
					
					<label class="radio-inline">
					 <input type="radio" name="optionsRadios"  id="lingua-ingles" value="Inglês" checked>    <label for="lingua-ingles">Inglês</label>
					</label>
					<label class="radio-inline">
					 <input type="radio" name="optionsRadios"  id="lingua-espanhol" value="Espanhol" checked>    <label for="lingua-espanhol">Espanhol</label>
					</label>
						
					
				</div>
			</div>
		</div>	

		<div class="row">	
			<div class="col-xs-12 col-md-3">
				<div class="checkbox">
					 <label class="admissao-form__label">
						<input type="checkbox" id="necessidade-especial" />    <label for="necessidade-especial">Necessidade especial?</label>
					</label>
					
				</div>
				
				<select class="form-control">
				  <option disabled selected>Qual?</option>
				  <option>2</option>
				  <option>3</option>
				  <option>4</option>
				  <option>5</option>
				</select>
			</div>
		</div>	
		<hr />
	</section>

	<section class="admissao-form-wrapper">

		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">CPF:</label><br />
					<input type="text" class="form-control " placeholder="CPF" />
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Senha:</label><br />
					<input type="password" class="form-control" placeholder="Senha" />
					
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Confirmar senha:</label><br />
					<input type="password" class="form-control" placeholder="Confirmar senha" />
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">RG:</label><br />
					<input type="text" class="form-control " placeholder="RG" />
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Órgão Emissor:</label><br />

					<select class="form-control admissao-input">
					  <option disabled="" selected="">Órgão Emissor</option>
					  <option>2</option>
					  <option>3</option>
					  <option>4</option>
					  <option>5</option>
					</select>
					
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Data de Emissão:</label><br />
					<input type="date" class="form-control" placeholder="Data de Emissão" />
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-xs-12">
					<button type="submit" class="btn btn-default admissao-submit-btn">AGENDAR PROVA</button>
			</div>	
		</div>
		<hr class="forma-de-ingresso__divider divider__enem" />
	</section>


	<!--=============================================================

					OPÇÃO CLICADA: ENEM

	-=============================================================-->

	<section class="admissao-form-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="admissao-form-title">INCLUA SUAS NOTAS DO ENEM</h1>
				<p class="admissao-form-description"><strong>Não poderão concorrer</strong> às vagas os candidatos que tiveram nota <strong>abaixo de 300 (trezentos)</strong> pontos na média das <strong>provas objetivas</strong> e abaixo de 300 na <strong>redação</strong>.</p>
			</div>
		</div>		
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Ciências Humanas e suas Tecnologias:</label><br />
					<input type="text" class="form-control " />
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Ciências da Natureza e suas Tecnologias:</label><br />
					<input type="text" class="form-control " />
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Linguagens, Códigos e suas Tecnologias:</label><br />
					<input type="text" class="form-control " />
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Matemática e suas Tecnologias:</label><br />
					<input type="text" class="form-control " />
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Nota da redação:</label><br />
					<input type="text" class="form-control " />
				</div>
			</div>
		
			<div class="col-xs-12 col-md-4">
				<div class="admissao-input">
					<div class="checkbox">
					<br />
					<label class="admissao-form__label">
						<input type="checkbox" id="informacoes-verdadeiras" />    <label for="informacoes-verdadeiras">Confirmo que os dados informados são verdadeiros.</label>
					</label>
				</div>
				</div>
			</div>
			
			<hr class="forma-de-ingresso__divider" />
		</div>	
	</section>

	<section class="admissao-form-wrapper">

		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">CPF:</label><br />
					<input type="text" class="form-control " placeholder="CPF" />
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Senha:</label><br />
					<input type="password" class="form-control" placeholder="Senha" />
					
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Confirmar senha:</label><br />
					<input type="password" class="form-control" placeholder="Confirmar senha" />
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">RG:</label><br />
					<input type="text" class="form-control " placeholder="RG" />
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Órgão Emissor:</label><br />

					<select class="form-control admissao-input">
					  <option disabled="" selected="">Órgão Emissor</option>
					  <option>2</option>
					  <option>3</option>
					  <option>4</option>
					  <option>5</option>
					</select>
					
				</div>
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">Data de Emissão:</label><br />
					<input type="date" class="form-control" placeholder="Data de Emissão" />
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-xs-12">
					<button type="submit" class="btn btn-default admissao-submit-btn">CONTINUAR</button>
			</div>
			<hr class="forma-de-ingresso__divider" />
		</div>
	</section>	
</section>


<?php require_once("footer.php"); ?>