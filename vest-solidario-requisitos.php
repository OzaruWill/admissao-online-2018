<!DOCTYPE html>
<html lang="br">

<head>
    <meta charset="utf-8" />
    <title>Processo de Admissão por Vestibular</title>

    <meta name="description" content="and Validation" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- basic styles -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="unisuam/font-awesome-4.6.3/css/font-awesome.min.css" />
    <link rel="stylesheet" href="assets/css/ace-fonts.css" />
    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="unisuam/css/style.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
    <![endif]-->
    <script src="assets/js/ace-extra.min.js"></script>

    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    
    <!-- Facebook Conversion Code for Vest - Checkin --> 
	<script>(function() { 
      var _fbq = window._fbq || (window._fbq = []); 
      if (!_fbq.loaded) { 
        var fbds = document.createElement('script'); 
        fbds.async = true; 
        fbds.src = 'https://connect.facebook.net/en_US/fbds.js'; 
        var s = document.getElementsByTagName('script')[0]; 
        s.parentNode.insertBefore(fbds, s); 
        _fbq.loaded = true; 
      } 
    })(); 
    window._fbq = window._fbq || []; 
    window._fbq.push(['track', '6019928844564', {'value':'0.00','currency':'USD'}]); 
    </script> 
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6019928844564&amp;cd%5Bvalue%5D=0.00&amp;cd%5Bcurrency%5D=USD&amp;noscript=1%22" ></noscript> 
</head>

<body class="fundo">

<div class="page-content fundo">
    <div class="row">
        <div class="col-xs-12">

            <div class="form-container">
                <div class="topo">
                    <h1 class="ajuste-logo">
                        <img src="unisuam/images/logo-unisuam-topo.png" />
                    </h1>
                    
                    <div class="modalidade">
                    	<img src="unisuam/images/modalidade-vest.jpg" />
                    </div>
                </div>
                
                <div class="topo space-bottom-35">
                	<img src="unisuam/images/banner-vestibular.jpg" class="img-responsive esconder-banner" />
                    <img src="unisuam/images/banner-vestibular-celular.jpg" class="img-responsive esconder-banner-celular" />
                </div>
                <!-------------------------------------- LOGIN ----------------------------------->
                <div class="position-relative">
                    <div id="login-box" class="login-box visible no-border">
                        <div class="widget-body">
                            <div class="widget-main pd-contrato">
                                <div class="page-header text-center">
                                    <h1>
                                        Inscrição para a Prova do <strong>VESTIBULAR SOLIDÁRIO</strong>
                                    </h1>
                                </div><!-- /.page-header -->
                                <!------------------------------------------------- FORMULÁRIO --------------------------------------------->




                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="center-block">
                                                <img src="unisuam/images/etapas/vest-3.jpg" class="center-block img-responsive" />
                                            </div>

                                            <hr />
                                            <div class="step-content row-fluid position-relative" id="step-container">
                                                <div class="step-pane active" id="step1">
                                                
                                                	<div class="alert alert-block alert-danger">
                                                        A(s) resposta(s) abaixo marcada(s) como "não" informa(m) que você não está apto para solicitar a bolsa. Todos os requisitos listados devem estar de acordo para que sua bolsa seja deferida.<br /> <br /> Mas não fique triste! Você ainda pode tentar o <a href="http://admissao.unisuam.edu.br/admissao/login/forma-ingresso/1">Vestibular Agendado ou Tradicional</a> ou usar sua nota do <a href="http://admissao.unisuam.edu.br/admissao/login/forma-ingresso/3">ENEM</a>* a partir de 2006, e ganhar 40% de bolsa durante todo o curso.<br />
                                                        <small>*Os candidatos devem ter no mínimo 300 (trezentos) pontos na prova objetiva e na redação do ENEM.</small>
                                                    </div>

                                                    <h3 class="smaller center">Confirme alguns pré-requisitos para a solicitação da Bolsa Solidária:</h3><br />
                                                       <center><span class="obrigatorio"><i class="ace-icon small-icon fa fa-asterisk"></i> Preenchimento obrigatório</span></center><br /> 
                                                        
                                                       <form class="form-horizontal" method="get">
                                                                                                                    
                                                          <div class="form-group">
                                                              <label class="col-xs-12 col-sm-6">A renda per-capta do seu grupo familiar é menor que R$1.300,00? <a data-rel="tooltip" href="#modal-percapta" data-toggle="modal" title="O que é renda per-capta?"><i class="fa fa-question-circle"></i></a></label>
              
                                                              <div class="col-xs-12 col-sm-6">
                                                                  <i class="ace-icon small-icon fa fa-asterisk obrigatorio-asterisco"></i>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Sim</span>
                                                                  </label>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Não</span>
                                                                  </label>
                                                              </div>
                                                          </div>
                                                          
                                                          <hr />
                                                          
                                                          <div class="form-group">
                                                              <label class="col-xs-12 col-sm-6">Você possui RG?</label>
              
                                                              <div class="col-xs-12 col-sm-6">
                                                                  <i class="ace-icon small-icon fa fa-asterisk obrigatorio-asterisco"></i>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Sim</span>
                                                                  </label>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Não</span>
                                                                  </label>
                                                              </div>
                                                          </div>
                                                          
                                                          <hr />
                                                          
                                                          <div class="form-group">
                                                              <label class="col-xs-12 col-sm-6">Você possui carteira de trabalho?</label>
              
                                                              <div class="col-xs-12 col-sm-6">
                                                                  <i class="ace-icon small-icon fa fa-asterisk obrigatorio-asterisco"></i>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Sim</span>
                                                                  </label>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Não</span>
                                                                  </label>
                                                              </div>
                                                          </div>
                                                          
                                                          <hr />
                                                          
                                                          <div class="form-group">
                                                              <label class="col-xs-12 col-sm-6">Você possui CPF?</label>
              
                                                              <div class="col-xs-12 col-sm-6">
                                                                  <i class="ace-icon small-icon fa fa-asterisk obrigatorio-asterisco"></i>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Sim</span>
                                                                  </label>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Não</span>
                                                                  </label>
                                                              </div>
                                                          </div>
                                                          
                                                          <hr />
                                                          
                                                          <div class="form-group">
                                                              <label class="col-xs-12 col-sm-6">Você possui Contracheque de todos que atualmente trabalham no seu grupo familiar?</label>
              
                                                              <div class="col-xs-12 col-sm-6">
                                                                  <i class="ace-icon small-icon fa fa-asterisk obrigatorio-asterisco"></i>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Sim</span>
                                                                  </label>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Não</span>
                                                                  </label>
                                                              </div>
                                                          </div>
                                                          
                                                          <hr />
                                                          
                                                          <div class="form-group">
                                                              <label class="col-xs-12 col-sm-6">Você possui Comprovante de Residência?</label>
              
                                                              <div class="col-xs-12 col-sm-6">
                                                                  <i class="ace-icon small-icon fa fa-asterisk obrigatorio-asterisco"></i>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Sim</span>
                                                                  </label>
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="radio" class="ace" />
                                                                      <span class="lbl"> Não</span>
                                                                  </label>
                                                              </div>
                                                          </div>
                                                          
                                                          <hr />
                                                          
                                                           <div class="form-group pull-left">
                                                              <div class="col-xs-12">
                                                                  <label class="pull-left margin-label">
                                                                      <input name="" type="checkbox" class="ace" />
                                                                      <span class="lbl"> Declaro que possuo os originais dos documentos acima e, caso seja aprovado no vestibular e convocado para a apresentação entregarei todos os solicitados de acordo com o <a href="#">edital de bolsas</a></span>
                                                                  </label>
                                                              </div>
                                                          </div>
                                                          
                                                      </form>
                                                    
                                                </div>
                                            </div>

                                            <hr />
                                            <div class="row-fluid wizard-actions">
                                                <button class="btn btn-danger btn-prev">
                                                    <i class="fa fa-arrow-left"></i>
                                                    Anterior
                                                </button>
                                                
                                                <button class="btn btn-primary btn-next" data-last="Concluir ">
                                                    Próxima
                                                    <i class="fa fa-arrow-right"></i>
                                                </button>
                                            </div>
                                        </div><!-- /widget-main -->
                                    </div><!-- /widget-body -->





			<div id="modal-percapta" class="modal" tabindex="-1">
              <div class="modal-dialog modal-mobile">
                  <div class="modal-content">
                      <div class="modal-header modal-header-mobile">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="blue bigger">O que é renda per-capta?</h4>
                      </div>

                      <div class="modal-body overflow-visible">
                          <div class="row margin-mobile-none">
                              <div class="col-xs-12">
                                 <p>Renda per-capta é o resultado da divisão do total de renda familiar pelo número de moradores de uma residência.</p>
                                 <p><strong>1º</strong> - Some todos os rendimentos dos familiares que residem em sua casa (salários, lucro e atividade agrícola, pensão, aposentadoria, etc).<br />
                                 <strong>2º</strong> - Em seguida, divida o resultado encontrado pelo número total de familiares que moram em sua casa, incluindo os que não têm renda.<br />
                                 <strong>3º</strong> - O valor encontrado será a renda per capita da sua família.</p>
                              </div>
                          </div>
                      </div>
                      
                      <div class="modal-footer modal-footer-mobile">
                          <button class="btn btn-sm btn-danger" data-dismiss="modal">
                              <i class="fa fa-close"></i>
                              Fechar
                          </button>
                      </div>
                      
                  </div>
                </div>
            </div> 











                                <!------------------------------------------------- FINAL - INSTITUICAO --------------------------------------------->
                            </div><!-- /widget-main -->
                        </div><!-- /widget-body -->
                    </div><!-- /forgot-box -->


                    <div class="row">
                        <div class="col-xs-12 login-info">
                            <hr />
                            <p><strong>Central de informações:</strong><br /> 21 3882-9797 | 21 3882-9752 | 21 98121-8095<br />
                                © 2016 UNISUAM. Todos os direitos reservados.</p>
                        </div>
                    </div>

                </div><!-- /position-relative -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.main-container -->




<!-- basic scripts -->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>
<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>
<!-- inline scripts related to this page -->

<script src="assets/js/jquery.maskedinput.min.js"></script>

</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-wizard.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 11 Apr 2014 00:52:08 GMT -->
</html>