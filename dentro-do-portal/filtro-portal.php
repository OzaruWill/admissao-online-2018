<section class="admissao-form-wrapper">
	<section class="container borda">
		<div class="area-filtro">
			<section class="container">
				<!-- CUT -->
				<div class="row" id="cut">
					<div class="row">
						<div class="col-lg-12 filtro-criterio">
							Quero escolher por: <button id="btn-cut-curso" class="btn-cut-enabled">CURSO</button> <button id="btn-cut-unidade" class="btn-cut-disabled">UNIDADE</button>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="campo">
							<span class="rotulo">Escolha o <strong>curso</strong></span><br>
							<input type="text" class="form-group" name="">
							<a href="#" id="btnCurso"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
						</div>
						<!-- campo -->
					</div>
					<!-- col -->
					<div class="col-lg-3">
						<div class="campo">
							<span class="rotulo">Escolha a <strong>unidade</strong></span><br>
							<input type="text" class="form-group" name="">
							<a href="#" id="btnUnidade"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
						</div>
						<!-- campo -->
					</div>
					<!-- col -->
					<div class="col-lg-3">
						<div class="campo">
							<span class="rotulo">Escolha o <strong>turno</strong></span><br>
							<input type="text" class="form-group" name="">
							<a href="#" id="btnTurno"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
						</div>
						<!-- campo -->
					</div>
					<!-- col -->
					<div class="col-lg-3">
						<button class="btn-filtrar">ESCOLHER</button>
					</div>
					<!-- col -->
				</div>
				<!-- row -->
				<!-- / CUT -->
				<!-- CURSOS -->
				<div id="cursos">
					<a href="#" id="btnFecharCursos">
					<i class="fa fa-times fa-2x fechar" aria-hidden="true"></i>
					</a>
					<div class="row">
						<h3>ESCOLHA O SEU CURSO</h3>
						<div class="linha"></div>
					</div>
					<!-- row -->
					<div class="row lista">
						<div class="col-lg-3">
							<ul class="lista">
								<li><a href="#">Administração</a></li>
								<li><a href="#">Análise e Desenv. de Sistemas</a></li>
								<li><a href="#">Arquitetura e Urbanismo</a></li>
								<li><a href="#">Biologia (Bacharelado)</a></li>
								<li><a href="#">Ciência da Computação</a></li>
								<li><a href="#">Ciências Biológicas (Lic)</a></li>
								<li><a href="#">Ciências Contábeis</a></li>
								<li><a href="#">Direito</a></li>
							</ul>
						</div>
						<!-- col -->
						<div class="col-lg-3">
							<ul>
								<li><a href="#">Educação Física (Bac)</a></li>
								<li><a href="#">Educação Física (Lic)</a></li>
								<li><a href="#">Enfermagem</a></li>
								<li><a href="#">Engenharia Civil</a></li>
								<li><a href="#">Engenharia de Produção</a></li>
								<li><a href="#">Engenharia Elétrica</a></li>
								<li><a href="#">Engenharia Mecânica</a></li>
								<li><a href="#">Estética e Cosmética</a></li>
							</ul>
						</div>
						<!-- col -->
						<div class="col-lg-3">
							<ul>
								<li><a href="#">Farmácia</a></li>
								<li><a href="#">Fisioterapia</a></li>
								<li><a href="#">Formação Pedagógica</a></li>
								<li><a href="#">Gastronomia</a></li>
								<li><a href="#">Gestão de Recursos Humanos</a></li>
								<li><a href="#">História</a></li>
								<li><a href="#">Jornalismo</a></li>
								<li><a href="#">Logística</a></li>
							</ul>
						</div>
						<!-- col -->
						<div class="col-lg-3">
							<ul>
								<li><a href="#">Marketing</a></li>
								<li><a href="#">Nutrição</a></li>
								<li><a href="#">Pedagogia</a></li>
								<li><a href="#">Processos Gerenciais</a></li>
								<li><a href="#">Psicologia</a></li>
								<li><a href="#">Publicidade</a></li>
								<li><a href="#">Serviço Social</a></li>
								<li><a href="#">Turismo (Bacharelado)</a></li>
							</ul>
						</div>
						<!-- col -->
					</div>
					<!-- row lista -->
				</div>
				<!-- cursos -->
				<!-- / CURSOS -->
				<!-- UNIDADES -->
				<div id="unidades">
					<a href="#" id="btnFecharUnidades">
					<i class="fa fa-times fa-2x fechar" aria-hidden="true"></i>
					</a>
					<div class="row">
						<h3>ESCOLHA A SUA UNIDADE</h3>
						<div class="linha"></div>
					</div>
					<!-- row -->
					<div class="row lista">
						<div class="col-lg-12 text-center">
							<ul>
								<li><a href="#">Bonsucesso</a></li>
								<li><a href="#">Campo Grande</a></li>
								<li><a href="#">Bangu</a></li>
								<li><a href="#">Jacarepaguá</a></li>
							</ul>
						</div>
						<!-- col -->
					</div>
					<!-- row lista -->
				</div>
				<!-- unidades -->
				<!-- / UNIDADES -->
				<!-- TURNOS -->
				<div id="turnos">
					<a href="#" id="btnFecharTurnos">
					<i class="fa fa-times fa-2x fechar" aria-hidden="true"></i>
					</a>
					<div class="row">
						<h3>ESCOLHA O MELHOR TURNO</h3>
						<div class="linha"></div>
					</div>
					<!-- row -->
					<div class="row lista">
						<div class="col-lg-12 text-center">
							<ul>
								<li><a href="#">Manhã</a></li>
								<li><a href="#">Tarde</a></li>
								<li><a href="#">Noite</a></li>
							</ul>
						</div>
						<!-- col -->
					</div>
					<!-- row lista -->
				</div>
				<!-- unidades -->
				<!-- / TURNOS -->
			</section>
			<!--/container-->
		</div>
		<!-- area-filtro -->
	</section>
</section>