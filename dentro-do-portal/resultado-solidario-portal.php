<section id="admissao">
	<section class="admissao-resultado-wrapper admissao-resultado__topo">
		<section class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="admissao-title">RESULTADO DO VEST</h1>
				</div>
			</div>
			
			<!-- STATUS APROVADO CLASSIFICADO -->
			<div class="row admissao-status-wrapper">
					<div class="col-xs-12 col-md-12 text-center">
						<div class="box-classificado">
						  <h2><i class="fa fa-check" aria-hidden="true"></i> Classificado!</h3>
						  Seu próximo passo é preencher seus dados pessoais e comparecer à secretaria da sua Unidade UNISUAM com todos os documentos solicitados em mãos até o dia xx/xx/xxxx. O não comparecimento ou não entrega de todos os documentos solicitados dentro do prazo determinado, implica em perda do direito da bolsa.
						</div>
					</div>
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__aprovado">
						<i class="fa fa-thumbs-o-up fa-3x"></i>
						<h1 class="">APROVADO</H1>
					</div>
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
						<h2 class="admissao-status__title">Nota da Prova Objetiva</h2>
						<label class="aprovado-green-circle">
							100
						</label>
					</div>
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
						<h2 class="admissao-status__title">Nota da Redação</h2>
						<label class="aprovado-green-circle">
							81
						</label>
					</div>
			</div>

			<!-- STATUS APROVADO NÃO CLASSIFICADO -->
			<div class="row admissao-status-wrapper">
					<div class="col-xs-12 col-md-12 text-center">
						<div class="box-nao-classificado">
						  <h2><i class="fa fa-frown-o" aria-hidden="true"></i> Não Classificado.</h3>
						  Infelizmente você ainda não está classificado para efetuar sua matrícula com a bolsa solidária, mas fique atento às datas do edital e ao seu e-mail para saber das novidades, pois pode haver reclassificação.
						</div>
					</div>
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__aprovado">
						<i class="fa fa-thumbs-o-up fa-3x"></i>
						<h1 class="">APROVADO</H1>
					</div>
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
						<h2 class="admissao-status__title">Nota da Prova Objetiva</h2>
						<label class="aprovado-green-circle">
							78
						</label>
					</div>
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
						<h2 class="admissao-status__title">Nota da Redação</h2>
						<label class="aprovado-green-circle">
							75
						</label>
					</div>
			</div>
			
			<!-- STATUS REPROVADO -->
			<div class="row admissao-status-wrapper">
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__reprovado">
						<i class="fa fa-frown-o fa-3x"></i>
						<h1 class="">REPROVADO</H1>
					</div>
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
						<h2 class="admissao-status__title">Nota da Prova Objetiva</h2>
						<label class="reprovado-red-circle">
							0
						</label>
					</div>
					<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
						<h2 class="admissao-status__title">Nota da Redação</h2>
						<label class="reprovado-red-circle">
							0
						</label>
					</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<button type="button" class="btn-imprimir-resultado center-block"><i class="fa fa-print"></i>  IMPRIMIR O RESULTADO</button>
				</div>
			</div>
		</section>
	</section>
	<section class="admissao-resultado-wrapper">
		<section class="container">

			<!-----------------MODAL---------------------->
								<div id="upload-doc" class="modal" tabindex="-1">
								  <div class="modal-dialog modal-mobile">
									  <div class="modal-content">
										  <div class="modal-header modal-header-mobile">
											  <button type="button" class="close" data-dismiss="modal">&times;</button>
											  <h4 class="blue bigger">CPF</h4>
										  </div>

											<div class="modal-body overflow-visible">
												<div class="row margin-mobile-none">
													<div class="col-xs-12 col-sm-12 margin-mobile-none">
														
														<h5>Clique abaixo para procurar o arquivo ou arraste-o para o trecho destacado.</h5>
														
														<div class="widget-body">
															<div class="widget-main">
																<input multiple="" type="file" id="id-input-file-3" />														
															</div>
														</div>
														
														<br />
														
														<h4>Arquivos enviados</h4>
														<table id="sample-table-1" class="table table-striped table-hover table-responsive space-top">
															<thead>
																<tr>
																	<th>Nome do arquivo</th>
																	<th>Data de envio</th>
																	<th>Excluir</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>cpf-mauricio-pacheco.pdf</td>
																	<td>00/00/0000 às 00h00 </td>
																	<td><button class="btn btn-xs btn-danger" data-toggle="modal"  href="#upload-doc"><i class="fa fa-trash bigger-120" aria-hidden="true"></i></button></td>
																</tr>
															</tbody>
														</table>
												  
													</div>
												</div>
											</div>

										  <div class="modal-footer modal-footer-mobile">
											  <button type="submit" class="btn btn-sm btn-primary">
												  <i class="fa fa-check"></i> 
												  Enviar para análise
											  </button>
											  
											  <button type="submit" class="btn btn-sm btn-danger" data-dismiss="modal">
												  <i class="fa fa-times"></i> 
												  Fechar sem enviar
											  </button>  
										  
										  </div>
									  </div>
								  </div>
							  </div><!-- MODAL -->
		</section>
	</section>
</section>