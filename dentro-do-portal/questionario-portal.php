<section class="container borda">
	<section class="admissao-form-wrapper">


		<div class="row">

			<div class="col-xs-12">
				<h1 class="admissao-form-title">LOGIN DO CANDIDATO</h1>
			</div>

			<div class="col-xs-12">

				<div class="alert alert-danger">
				 A(s) resposta(s) abaixo marcada(s) como "não" informa(m) que você não está apto para solicitar a bolsa. Todos os requisitos listados devem estar de acordo para que sua bolsa seja deferida.<br /> <br /> Mas não fique triste! Você ainda pode tentar o <a href="#" class="blue-link">Vestibular Agendado ou Tradicional</a> ou usar sua nota do <a href="#" class="blue-link">ENEM</a>* a partir de 2006, e ganhar 40% de bolsa durante todo o curso.<br />
                 <small>*Os candidatos devem ter no mínimo 300 (trezentos) pontos na prova objetiva e na redação do ENEM.</small>
             	</div>

			</div><!-- col -->
			
		</div><!-- row -->


		<div class="row">

			<div class="col-xs-12">
				<p class="admissao-form-description">Confirme alguns pré-requisitos para a solicitação da Bolsa Solidária:</p>
				<p class="admissao-form-description">* Preenchimento obrigatório</p>

			</div><!-- col -->
			
		</div><!-- row -->


		<div class="row">

			<div class="col-xs-12 admissao-form-description">

				

				<table width="100%" class="table" border="0" cellspacing="0" cellpadding="0">


				  <tr>
				    <td align="left">A <a href="#" data-toggle="modal" data-target="#myModal" style="text-decoration: underline;">renda per-capta</a> do seu grupo familiar é menor que R$1.431,00?</td>
				    <td>
						<label class="radio-inline">
						 <input type="radio" name="renda" id="renda-sim" value="Sim" checked="">    <label for="renda-sim">Sim</label>
						</label>
						
						<label class="radio-inline">
						 <input type="radio" name="renda" id="renda-nao" value="Não" checked="">    <label for="renda-nao">Não</label>
						</label>
				    </td>
				  </tr>

				  <tr>
				    <td align="left">Você possui RG?</td>
				    <td>
						<label class="radio-inline">
						 <input type="radio" name="rg" id="rg-sim" value="Sim" checked="">    <label for="rg-sim">Sim</label>
						</label>
						
						<label class="radio-inline">
						 <input type="radio" name="rg" id="rg-nao" value="Não" checked="">    <label for="rg-nao">Não</label>
						</label>
				    </td>
				  </tr>

				  <tr>
				    <td align="left">Você possui carteira de trabalho?</td>
				    <td>
						<label class="radio-inline">
						 <input type="radio" name="clt" id="clt-sim" value="Sim" checked="">    <label for="clt-sim">Sim</label>
						</label>
						
						<label class="radio-inline">
						 <input type="radio" name="clt" id="clt-nao" value="Não" checked="">    <label for="clt-nao">Não</label>
						</label>
				    </td>
				  </tr>

				  <tr>
				    <td align="left">Você possui CPF?</td>
				    <td>
						<label class="radio-inline">
						 <input type="radio" name="cpf" id="cpf-sim" value="Sim" checked="">    <label for="cpf-sim">Sim</label>
						</label>
						
						<label class="radio-inline">
						 <input type="radio" name="cpf" id="cpf-nao" value="Não" checked="">    <label for="cpf-nao">Não</label>
						</label>
				    </td>
				  </tr>

				  <tr>
				    <td align="left">Você possui Contracheque de todos que atualmente trabalham no seu grupo familiar?</td>
				    <td>
						<label class="radio-inline">
						 <input type="radio" name="contracheque" id="contracheque-sim" value="Sim" checked="">    <label for="contracheque-sim">Sim</label>
						</label>
						
						<label class="radio-inline">
						 <input type="radio" name="contracheque" id="contracheque-nao" value="Não" checked="">    <label for="contracheque-nao">Não</label>
						</label>
				    </td>
				  </tr>

				  <tr>
				    <td align="left">Você possui Comprovante de Residência?</td>
				    <td>
						<label class="radio-inline">
						 <input type="radio" name="residencia" id="residencia-sim" value="Sim" checked="">    <label for="residencia-sim">Sim</label>
						</label>
						
						<label class="radio-inline">
						 <input type="radio" name="residencia" id="residencia-nao" value="Não" checked="">    <label for="residencia-nao">Não</label>
						</label>
				    </td>
				  </tr>

				</table>

				<div class="checkbox" style="text-align: left !important; font-size: 1.6rem !important;">
					<br>
					<label class="admissao-form__label">
						<input type="checkbox" id="informacoes-verdadeiras">    <label for="informacoes-verdadeiras">Declaro que possuo os originais dos documentos acima e, caso seja aprovado no vestibular e convocado para a apresentação entregarei todos os solicitados de acordo com o <a href="https://www.unisuam.edu.br/wp-content/uploads/2014/07/edital_solidario_20182.pdf" target="_blank" class="blue-link">edital de bolsas</a>.</label>
					</label>
				</div>

			</div><!-- col -->
			
		</div><!-- row -->


	</section>
</section>


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" class="">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">O que é Renda Per-capita?</h4>
        </div>
        <div class="modal-body">
          <p>Renda per-capta é o resultado da divisão do total de renda familiar pelo número de moradores de uma residência.</p>
          <p>1º - Some todos os rendimentos dos familiares que residem em sua casa (salários, lucro e atividade agrícola, pensão, aposentadoria, etc).</p>
          <p>2º - Em seguida, divida o resultado encontrado pelo número total de familiares que moram em sua casa, incluindo os que não têm renda.</p>
          <p>3º - O valor encontrado será a renda per capita da sua família.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
        </div>
      </div>
      
    </div>
  </div>