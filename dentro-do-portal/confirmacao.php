<section class="container borda">
	<section class="admissao-form-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="admissao-form-title">CONFIRMAÇÃO</h1>
				<p class="admissao-form-description">Confirme o curso, a unidade e o turno no qual você deseja se inscrever:</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-lg-3">
				<div class="admissao-input">
					<!--<label class="admissao-form__label">Curso:</label>-->
					<select class="form-control admissao-input">
						<option disabled selected>Curso</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-lg-3">
				<div class="admissao-input">
					<!--<label class="admissao-form__label">Unidade:</label>-->
					<select class="form-control admissao-input">
						<option disabled selected>Unidade</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-lg-3">
				<div class="admissao-input">
					<!--<label class="admissao-form__label ">Turno:</label>-->
					<select class="form-control admissao-input">
						<option disabled selected>Turno</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-lg-3">
				<button type="button" class="btn btn-default admissao-submit-btn">ENVIAR</button>
			</div>
		</div>
		<hr />
		<!--<div class="bg-cinza">-->
			<div class="row" style="margin: 20px 0 20px 0;">
				<h1 class="admissao-form-title">ADMINISTRAÇÃO</h1>
			</div>
			<!-- row -->
			<div class="row curso-text">
				<div class="col-md-5">
					<div class="curso-titulo-cinza">Sua escolha:</div>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 20px;">
						<tr>
							<td width="30%" class="curso-texto-light">Unidade</td>
							<td width="30%" class="curso-texto-laranja"><strong>Bonsucesso</strong></td>
							<td width="40%">
								<button class="btn-trocar"><i class="fa fa-exchange" aria-hidden="true"></i> Trocar</button>
							</td>
						</tr>
						<tr>
							<td class="curso-texto-light">Turno</td>
							<td class="curso-texto-laranja"><strong>Noite</strong></td>
							<td>
								<button class="btn-trocar"><i class="fa fa-exchange" aria-hidden="true"></i> Trocar</button>
							</td>
						</tr>
					</table>
					<br />
					<div class="admissao-input" style="margin-bottom: 0;">
						<label class="curso-titulo-cinza">Modalidade:</label>
						<label class="radio">
							<input type="radio" name="optionsRadios"  id="radio-presencial" value="Presencial" checked>    <label for="radio-presencial" ><strong>Presencial</strong>&nbsp;  Por <span class="curso-texto-laranja"><strong>1320,<small>00</small></strong></span> /mês.</label>
						</label>
						<label class="radio">
							<input type="radio" name="optionsRadios"  id="radio-flex" value="Flex">    <label for="radio-flex"><strong>Flex</strong>&nbsp;  Por <span class="curso-texto-laranja"><strong>660,<small>00</small></strong></span> /mês.</label></label>
						</label>
						<div class="radio disabled">
							<input type="radio" name="optionsRadios"  id="radio-ead" value="À distância" disabled>    <label for="radio-ead"><strong>À distância</strong>&nbsp;  Por <span class="curso-texto-laranja"><strong>520,<small>00</small></strong></span> /mês. | Não disponível no momento.</label>
						</div>
					</div>
					<!--<span class="curso-texto-old">De 1.088,75/mês</span>Por<span class="curso-texto-laranja"> <strong>660,00</strong></span><span style="font-weight: normal;">/mês.</span>-->
					<br />
					<div>
						<button type="submit" class="btn btn-default admissao-submit-btn" style="margin-bottom: 5px;">INSCREVA-SE</button>
					</div>
					<!--<div class="obs-texto">* Estimativa para 2018 sem a promoção, considerando cerca de 8% de reajuste anual.</div>-->
				</div>
				<!-- col -->
				<div class="col-md-3">
					<div class="curso-info">
						<i class="fa fa-user fa-2x" aria-hidden="true"></i>
						<h5>Coordenação:</h5>
						Prof. Josué José da Silva
					</div>
					<div class="curso-info">
						<i class="fa fa-calendar fa-2x" aria-hidden="true"></i>
						<h5>Duração Estimada:</h5>
						4 anos (8 períodos)
					</div>
				</div>
				<!-- col -->
				<div class="col-md-4">
					<ul class="nav nav-tabs nav-justified">
						<li id="estilo-disciplinas"><a href="#" id="btn-disciplinas">DISCIPLINAS</a></li>
						<li id="estilo-corpo-docente" class="active"><a href="#" id="btn-corpo-docente">CORPO DOCENTE</a></li>
					</ul>
					<br>
					<div class="content-tabela">
						<table class="table table-striped texto-tabela no-border" id="tab-corpo-docente">
							<thead>
								<tr class="titulo-tabela">
									<th>Professor</th>
									<th>Titulação</th>
									<th class="text-center">Lattes</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Alan Vieira de Almeida</td>
									<td>Mestrado</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td>Alessandro Marinho Pinheiro</td>
									<td>Mestrado</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td>Alexandre Carreira de Souza</td>
									<td>Especialização</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td>Alexandre de Almeida Lima</td>
									<td>Mestrado</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td>André Luiz Marques Gomes</td>
									<td>Mestrado</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td>Augusto Cesar Dias</td>
									<td>Mestrado</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td>Carlos Alexandre Duarte Correa</td>
									<td>Mestrado</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td>Carlos Eduardo José da Silva</td>
									<td>Mestrado</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td>Claudia Val dos Santos</td>
									<td>Mestrado</td>
									<td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
								</tr>
							</tbody>
						</table>
						<table class="table table-striped texto-tabela no-border" id="tab-disciplinas">
							<thead>
								<tr class="titulo-tabela">
									<th>Disciplinas</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Metodologia do Trabalho Acadêmico e Científico</td>
								</tr>
								<tr>
									<td>Contabilidade Básica</td>
								</tr>
								<tr>
									<td>Fundamentos da Gestão</td>
								</tr>
								<tr>
									<td>Matemática Aplicada</td>
								</tr>
								<tr>
									<td>Leitura e Produção de Textos</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- content-tabela -->
					<div style="clear:both;"></div>
				</div>
				<!-- col -->
			</div>
			<!-- row -->
		<!--</div>-->
		<!-- bg-cinza -->
	</section>
</section>