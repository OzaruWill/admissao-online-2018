
jQuery(document).ready(function(){

	jQuery('#btnCurso').click(function(e){
		jQuery('#cut').css("display","none");
		jQuery('#cursos').css("display","block");
		
		 e.preventDefault();
	});

	jQuery('#btnFecharCursos').click(function(e){
		jQuery('#cut').css("display","block");
		jQuery('#cursos').css("display","none");
		
		e.preventDefault();
	});

	jQuery('#btnUnidade').click(function(e){
		jQuery('#cut').css("display","none");
		jQuery('#unidades').css("display","block");
		
		e.preventDefault();
	});

	jQuery('#btnFecharUnidades').click(function(e){
		jQuery('#cut').css("display","block");
		jQuery('#unidades').css("display","none");
		
		e.preventDefault();
	});

	jQuery('#btnTurno').click(function(e){
		jQuery('#cut').css("display","none");
		jQuery('#turnos').css("display","block");
		
		e.preventDefault();
	});

	jQuery('#btnFecharTurnos').click(function(e){
		jQuery('#cut').css("display","block");
		jQuery('#turnos').css("display","none");
		
		e.preventDefault();
	});

	jQuery('#btn-disciplinas').click(function(e){
		jQuery('#tab-disciplinas').css("display","block");
		jQuery('#tab-corpo-docente').css("display","none");
		jQuery('#estilo-disciplinas').addClass('active');
		jQuery('#estilo-corpo-docente').removeClass('active');
		
		e.preventDefault();
	});

	jQuery('#btn-corpo-docente').click(function(e){
		jQuery('#tab-disciplinas').css("display","none");
		jQuery('#tab-corpo-docente').css("display","block");
		jQuery('#estilo-disciplinas').removeClass('active');
		jQuery('#estilo-corpo-docente').addClass('active');
		
		e.preventDefault();
	});

	jQuery('#btn-cut-curso').click(function(e){
		jQuery(this).addClass('btn-cut-enabled');
		jQuery(this).removeClass('btn-cut-disabled');
		jQuery('#btn-cut-unidade').addClass('btn-cut-disabled');
		jQuery('#btn-cut-unidade').removeClass('btn-cut-enabled');
		
		e.preventDefault();
	});

	jQuery('#btn-cut-unidade').click(function(e){
		jQuery(this).addClass('btn-cut-enabled');
		jQuery(this).removeClass('btn-cut-disabled');
		jQuery('#btn-cut-curso').addClass('btn-cut-disabled');
		jQuery('#btn-cut-curso').removeClass('btn-cut-enabled');
		
		e.preventDefault();
	});

});