<?php require_once("header.php"); ?>

<section class="container">
	<section class="admissao-form-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="admissao-form-title">LOGIN DO CANDIDATO</h1>
			</div>
			<div class="col-xs-12 col-md-3 col-md-offset-3">
				<div class="input-group admissao-input">
					<label class="admissao-form__label">CPF:</label><br />
					<input type="text" class="form-control admissao-input" placeholder="CPF" />
				 </div>
				 <div class="input-group admissao-input">
					<label class="admissao-form__label">Senha:</label><br />
				  <input type="password" class="form-control admissao-input" placeholder="Senha" />
				</div>
				<button type="button" class="btn btn-default admissao-submit-btn">ENTRAR</button>
				<a href="#"><p class="esqueci-senha">Esqueci minha senha</p></a>
			</div>
			<div class="col-xs-12 col-md-3">
				<p class="admissao-login-text">Faça o login para <strong><span class="laranja">ver o resultado do VEST</span></strong> ou <strong><span class="laranja">continuar sua inscrição</span></strong> por ENEM, Portador de Diploma ou Transferência.</p>
				<p class="admissao-login-text">Se você ainda não escolheu seu curso e quer iniciar uma nova inscrição, clique abaixo.</p>
				<button class="btn-nova-inscricao">NOVA INSCRIÇÃO</strong>
			</div>
			
		</div>
	</section>
</section>


<?php require_once("footer.php"); ?>