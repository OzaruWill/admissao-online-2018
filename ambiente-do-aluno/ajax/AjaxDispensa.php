<?php

class AjaxDispensa {

    private $pdo;

    public function __construct(){
        $PDO_HOST     = 'bancoproducao.unisuam.edu.br';
        $PDO_DATABASE = 'saga';
        $PDO_USER     = 'saga';
        $PDO_PASSWORD = '486UNIs4g4SUAM351';

        $this->pdo	      = new PDO("pgsql:host={$PDO_HOST};dbname={$PDO_DATABASE};user={$PDO_USER};password='{$PDO_PASSWORD}'");

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function ajaxEmpresas(){

        $query = "SELECT bas.empresas.*, nome as descricao FROM bas.empresas
                    WHERE  bas.empresas.tipo = 2 ORDER BY BAS.empresas.nome";

        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll();

        function toUtf8(&$v) {
            $v = utf8_encode($v);
        }

        array_walk_recursive($results, 'toUtf8');

        return json_encode($results);
    }

    public function ajaxDisciplinaExterna(){

        $query = "SELECT gra.disciplinas_externas.codigo,
                         gra.disciplinas_externas.label as codigo_label,
                         bas.empresas.nome as nome_empresa,
                         gra.disciplinas_externas.nome as nome,
                         gra.disciplinas_externas.nome as label,
                         gra.disciplinas_externas.nome as \"desc\",
                         gra.disciplinas_externas.ies as ies
                    FROM gra.disciplinas_externas
                    JOIN bas.empresas ON bas.empresas.codigo = gra.disciplinas_externas.ies
                      WHERE ies = {$_REQUEST['id']}";

        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll();

        function toUtf8(&$v) {
            $v = utf8_encode($v);
        }

        array_walk_recursive($results, 'toUtf8');

        $array['codigo']     = $results;
        $array['disciplina'] = $results;

        foreach($array['codigo'] as &$r){
            $r['label'] = $r['codigo_label'];
        }



        return json_encode($array);
    }
}

$cmd = $_REQUEST['cmd'];
$ajax = new AjaxDispensa();
echo $ajax->$cmd();