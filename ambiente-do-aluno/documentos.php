<?php require_once("header-ambiente.php"); ?>

<section class="container">

<section class="admissao-docs-wrapper">
	<div class="row">
		<div class="col-xs-12">
			<img class="img-responsive center-block" src="unisuam/images/logo-unisuam-topo.png" alt="UNISUAM" />
		
			<h1 class="admissao-docs-title">AGORA FALTA POUCO</h1>
			<p class="admissao-form-description">Para concluir sua matrícula, basta efetuar o pagamento da primeira parcela e enviar seus documentos para análise.</p>
		</div>
	</div>
	<div class="row pagamento-info-wrapper">
			<div class="col-xs-12 col-md-8 pagamento-texto">
				<p class="pagamento-texto__p"><strong>Sua escolha de pagamento foi:</strong></p>
				<p class="pagamento-texto__p">8x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</p>
			</div>
			<div class="col-xs-12 col-md-4">
				<button class="admissao-submit-btn">IR PARA PAGAMENTO</button>
			</div>
	</div>
</section>

<section class="admissao-docs-wrapper">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="admissao-docs-subtitle">Envio de documentos  &nbsp;&nbsp; <button type="button" class="btn-como-fazer" data-toggle="modal" data-target="#tutorial" data-placement="bottom" title="Tooltip on bottom"><i class="fa fa-question-circle"></i> COMO FAZER?</button></h2>
			<hr />
			
			<div class="table-responsive">
				<table id="sample-table-1" class="table table-striped table-hover  admissao-docs__tabela">
						<tr>
							<th>Documento</th>
							<th>Parecer do atendente</th>
							<th>Envio</th>
							<th>Status</th>
						</tr>

						<tr>
							<td>CPF</td>
							<td></td>
							<td><button class="btn btn-xs btn-inverse" data-toggle="modal"  href="#upload-doc"><i class="fa fa-folder-open bigger-120" aria-hidden="true"></i></button></td>
							<td><span class="label label-sm label-warning admissao-label">Em análise</span></td>
						</tr>
						
						<tr>
							<td>RG</td>
							<td></td>
							<td><button class="btn btn-xs btn-inverse" data-toggle="modal"  href="#upload-doc"><i class="fa fa-folder-open bigger-120" aria-hidden="true"></i></button></td>
							<td><span class="label label-sm label-success admissao-label">Aprovado</span></td>
						</tr>
						
						<tr>
							<td>Diploma do Ensino Médio</td>
							<td>Imagem enviada está embaçada. É preciso enviar novas imagens com boa legibilidade.</td>
							<td><button class="btn btn-xs btn-inverse" data-toggle="modal"  href="#upload-doc"><i class="fa fa-folder-open bigger-120" aria-hidden="true"></i></button></td>
							<td><span class="label label-sm label-danger admissao-label">Reprovado</span></td>
						</tr>
						
						<tr>
							<td>Histórico do Ensino Médio</td>
							<td></td>
							<td><button class="btn btn-xs btn-inverse" data-toggle="modal"  href="#upload-doc"><i class="fa fa-folder-open bigger-120" aria-hidden="true"></i></button></td>
							<td><span class="label label-sm label-inverse admissao-label">Não enviado</span></td>
						</tr>
				</table>
			</div><!-- /.table-responsive -->
	
		</div>
	</div>
</section>






<!-----------------MODAL---------------------->
					<div id="upload-doc" class="modal" tabindex="-1">
					  <div class="modal-dialog modal-mobile">
						  <div class="modal-content">
							  <div class="modal-header modal-header-mobile">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4 class="blue bigger">CPF</h4>
							  </div>

								<div class="modal-body overflow-visible">
									<div class="row margin-mobile-none">
										<div class="col-xs-12 col-sm-12 margin-mobile-none">
											
											<h5>Clique abaixo para procurar o arquivo ou arraste-o para o trecho destacado.</h5>
											
											<div class="widget-body">
												<div class="widget-main">
													<input multiple type="file" id="id-input-file-3" />														
												</div>
											</div>
											
											<br />
											
											<h4>Arquivos enviados</h4>
											<table id="sample-table-1" class="table table-striped table-hover table-responsive space-top">
												<thead>
													<tr>
														<th>Nome do arquivo</th>
														<th>Data de envio</th>
														<th>Excluir</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>cpf-mauricio-pacheco.pdf</td>
														<td>00/00/0000 às 00h00 </td>
														<td><button class="btn btn-xs btn-danger" data-toggle="modal"  href="#upload-doc"><i class="fa fa-trash bigger-120" aria-hidden="true"></i></button></td>
													</tr>
												</tbody>
											</table>
									  
										</div>
									</div>
								</div>

							  <div class="modal-footer modal-footer-mobile">
								  <button type="submit" class="btn btn-sm btn-primary">
									  <i class="fa fa-check"></i> 
									  Enviar para análise
								  </button>
								  
								  <button type="submit" class="btn btn-sm btn-danger" data-dismiss="modal">
									  <i class="fa fa-times"></i> 
									  Fechar sem enviar
								  </button>  
							  
							  </div>
						  </div>
					  </div>
				  </div><!-- MODAL -->
</section>
<?php require_once("footer-ambiente.php"); ?>






<!-- Modal Tutorial -->
<div id="tutorial" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Como enviar documentos</h4>
      </div>
      <div class="modal-body pagamento-texto__p">

		<p>1. Para iniciar o upload de documentos, clique no ícone de envio;</p>
		<p><img style="border: solid 1px #EF7D00;" src="http://hotsite.unisuam.edu.br/admissao-online-2018/img/tutorial_01.png"></p>
		<p>2. A janela para escolha do arquivo se abrirá;</p>
		<p>3. Busque o local onde o arquivo se encontra;</p>
		<p><img style="border: solid 1px #EF7D00;" src="http://hotsite.unisuam.edu.br/admissao-online-2018/img/tutorial_02.png"></p>
		<p>4. Selecione o arquivo de até 8MB e clique em Enviar para Análise.</p>
		<p><img style="border: solid 1px #EF7D00;" src="http://hotsite.unisuam.edu.br/admissao-online-2018/img/tutorial_03.png"></p>
		<p><strong>Pronto. Seu envio de documento será concluído!</strong></p>
        
	</div>
		<div class="modal-footer">
			<button type="submit" class="btn btn-sm btn-danger" data-dismiss="modal">
				<i class="fa fa-times"></i> Fechar
			</button>
		</div>
	</div>

  </div>