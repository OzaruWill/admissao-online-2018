<!DOCTYPE html>
<html lang="br">

<head>
    <meta charset="utf-8" />
    <title>Processo de Admissão por Vestibular</title>

    <meta name="description" content="and Validation" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- Cole esse código o mais alto possível na tag HEAD da página: -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5CDT7BP');</script>
    <!-- End Google Tag Manager -->
    <!-- Essa deve ser inserida dentro da tag HEAD -->

    <!-- basic styles -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="unisuam/font-awesome-4.6.3/css/font-awesome.min.css" />
    <link rel="stylesheet" href="assets/css/ace-fonts.css" />
    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="unisuam/css/style.css" />
	<link rel="stylesheet" href="../style.css" />
	
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
    <![endif]-->
    <script src="assets/js/ace-extra.min.js"></script>

    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    
</head>

<body class="fundo">

<!-- Além disso, cole esse código imediatamente após a tag de abertura <body> -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5CDT7BP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

		<div class="topbar">
			<div class="navbar-header pull-left simbolo-unisuam">
				<small><img src="unisuam/images/unisuam-coruja.png" /></small>				
			</div>
		
            <div class="navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">
                    	
                    <li class="transparent">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <span>
                                <i class="fa fa-user icon-animated-hand-pointer" aria-hidden="true"></i> <small>Bem-vindo,</small> Mauricio
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </a>
    
                        <ul class="user-menu dropdown-blue pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                            <li class="dropdown-header">
                                Mauricio Gonçalves Fernandes Pacheco
                            </li>
                            <li class="grey dados-topo">CPF: <strong>000.000.000-00</strong></li>
							<li class="grey dados-topo">Curso: <strong>Administração</strong></li>
							<li class="grey dados-topo">Unidade: <strong>Bonsucesso</strong></li>
							<li class="grey dados-topo">Turno: <strong>Noite</strong></li>
                            <li class="grey dados-topo"><small>Inscrição por</small> <strong>Vestibular</strong></li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-edit"></i>
                                    Editar dados
                                </a>
                            </li>
    
                            <li>
                                <a href="#">
                                    <i class="fa fa-power-off red"></i>
                                    <span class="red no-hover-underline">Sair</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
		</div>