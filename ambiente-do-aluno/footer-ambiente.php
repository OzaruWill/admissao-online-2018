 <div class="row">
	<div class="col-xs-12 login-info">
		<hr />
		<p><strong>Central de informações:</strong><br /> 21 3882-9797 | 21 3882-9752<br />
			© 2018 UNISUAM. Todos os direitos reservados.</p>
	</div>
</div>

<!-- FOOTER SECTION - Before closing </body> tag -->

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="script.js"></script>


<!-- basic scripts -->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>
<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="http://admissao.unisuam.edu.br/assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="http://admissao.unisuam.edu.br/assets/js/fuelux/fuelux.wizard.min.js"></script>
<!-- ace scripts -->
<script src="http://admissao.unisuam.edu.br/assets/js/ace-elements.min.js"></script>
<script src="http://admissao.unisuam.edu.br/assets/js/ace.min.js"></script>
<!-- inline scripts related to this page -->
<script src="http://admissao.unisuam.edu.br/assets/js/jquery.inputlimiter.1.3.1.min.js"></script>

<script src="http://admissao.unisuam.edu.br/assets/js/jquery.maskedinput.min.js"></script>

<script type="text/javascript">
    jQuery(function($) {
        var $validation = false;
        $('#passo-a-passo').ace_wizard().on('change' , function(e, info){
            if(info.step == 1 && $validation) {
                if(!$('#validation-form').valid()) return false;
            }
        }).on('finished', function(e) {
            bootbox.dialog({
                message: "Thank you! Your information was successfully saved!",
                buttons: {
                    "success" : {
                        "label" : "OK",
                        "className" : "btn-sm btn-primary"
                    }
                }
            });
        }).on('stepclick', function(e){
            //return false;//prevent clicking on steps
        });
    });
	
	$.mask.definitions['~']='[+-]';
				$('.input-mask-date').mask('99/99/9999');
				$('.input-mask-cell').mask('(99) 99999-9999');
				$('.input-mask-tel').mask('(99) 9999-9999');
				$('.input-mask-cep').mask('99.999-999');
				$('.input-mask-cpf').mask('999.999.999-99');
				$('.input-mask-rg').mask('99.999.999-9');
				$('.input-mask-eleitor').mask('9999.9999.9999');
				$('.input-mask-zona').mask('999');
				$('.input-mask-secao').mask('9999');
	
				
				$('#id-input-file-3').ace_file_input({
					style:'well',
					btn_choose:'Clique ou arraste o arquivo aqui para enviar',
					btn_change:null,
					no_icon:'fa-cloud-upload',
					droppable:true,
					thumbnail:'small'//large | fit
					//,icon_remove:null//set null, to hide remove/reset button
					/**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
					/**,before_remove : function() {
						return true;
					}*/
					,
					preview_error : function(filename, error_code) {
						//name of the file that failed
						//error_code values
						//1 = 'FILE_LOAD_FAILED',
						//2 = 'IMAGE_LOAD_FAILED',
						//3 = 'THUMBNAIL_FAILED'
						//alert(error_code);
					}
			
				}).on('change', function(){
					//console.log($(this).data('ace_input_files'));
					//console.log($(this).data('ace_input_method'));
				});
				
			
				
</script>

</body>
</html>