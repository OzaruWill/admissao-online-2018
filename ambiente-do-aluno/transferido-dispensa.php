<!DOCTYPE html>
<html lang="br">

<head>
    <meta charset="utf-8" />
    <title>Processo de Admissão por Transferência</title>

    <meta name="description" content="and Validation" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- basic styles -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="unisuam/font-awesome-4.6.3/css/font-awesome.min.css" />
    <link rel="stylesheet" href="assets/css/ace-fonts.css" />
    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="unisuam/css/style.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
    <![endif]-->
    <script src="assets/js/ace-extra.min.js"></script>

    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    
    <!-- Facebook Conversion Code for Vest - Checkin --> 
	<script>(function() { 
      var _fbq = window._fbq || (window._fbq = []); 
      if (!_fbq.loaded) { 
        var fbds = document.createElement('script'); 
        fbds.async = true; 
        fbds.src = 'https://connect.facebook.net/en_US/fbds.js'; 
        var s = document.getElementsByTagName('script')[0]; 
        s.parentNode.insertBefore(fbds, s); 
        _fbq.loaded = true; 
      } 
    })(); 
    window._fbq = window._fbq || []; 
    window._fbq.push(['track', '6019928844564', {'value':'0.00','currency':'USD'}]); 
    </script> 
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6019928844564&amp;cd%5Bvalue%5D=0.00&amp;cd%5Bcurrency%5D=USD&amp;noscript=1%22" ></noscript> 
</head>

<body class="fundo">

<div class="page-content fundo">
    <div class="row">
        <div class="col-xs-12">

            <div class="form-container">
                <div class="topo">
                    <h1 class="ajuste-logo">
                        <img src="unisuam/images/logo-unisuam-topo.png" />
                    </h1>
                    
                    <div class="modalidade">
                    	<img src="unisuam/images/modalidade-transf-port.jpg" />
                    </div>
                </div>
                
                <div class="topo space-bottom-35">
                	<img src="unisuam/images/banner-transferido.jpg" class="img-responsive" />
                </div>
                <!-------------------------------------- LOGIN ----------------------------------->
                <div class="position-relative">
                    <div id="login-box" class="login-box visible no-border">
                        <div class="widget-body">
                            <div class="widget-main pd-contrato">
                                <div class="page-header text-center">
                                    <h1>
                                        Inscrição por <strong>TRANSFERÊNCIA</strong>
                                    </h1>
                                </div><!-- /.page-header -->
                                <!------------------------------------------------- FORMULÁRIO --------------------------------------------->




                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="center-block">
                                                <img src="unisuam/images/etapas/portador-3.jpg" class="center-block img-responsive" />
                                            </div>

                                            <hr />
                                            <div class="step-content row-fluid position-relative" id="step-container">
                                                <div class="step-pane active" id="step1">

                                                    <h3 class="smaller center">Dispensa de Disciplinas</h3><br />
                                                       
                                                        <p>
                                                            Adicione as disciplinas cursadas em outra Instituição de Ensino e simule suas dispensas para a matrícula na UNISUAM.
                                                        </p>
                                                        
                                                        <div class="alert alert-warning">
                                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Atenção!</strong>
Os campos abaixo são de preenchimento obrigatório. Caso o código e/ou o nome da disciplina não constem na lista fornecida, é possível inserir o nome da disciplina manualmente no respectivo campo e adicioná-la clicando no botão Adicionar.
Selecione a Instituição de Ensino. Caso não seja encontrada na lista, clique em avançar e siga com o seu cadastro.
														</div>
                                                        
                                                       <form class="form-horizontal" method="get">
                                                          <div class="form-group">
                                                              <label class="control-label col-xs-12 col-sm-4 no-padding-right">Instituição de Ensino:</label>
              
                                                              <div class="col-xs-12 col-sm-8">
                                                                  <select id="" class="col-xs-12 col-sm-9 col-md-9 col-lg-9" type="select" required>
                                                                    <option>Selecione</option>
                                                                </select>
                                                              </div>
                                                          </div>
                                                          
                                                          <div class="form-group">
                                                            <label class="control-label col-xs-12 col-sm-4 no-padding-right">Nome da Disciplina:</label>
            
                                                            <div class="col-xs-12 col-sm-8">
                                                                <input type="text" id="" name="" class="col-xs-12 col-sm-8 col-md-9 col-lg-9" />
                                                            </div>
                                                          </div>
                                                      	</form>
                                                        
                                                        
                                                        
                                                        
                                                        <!-------------------------- APARECER SE NÃO ENCONTRAR A DISCIPLINA ------------------------------>
                                                        
                                                        <br /><br />
                                                        <h5 class="blue pull-left">Lista de disciplinas que podem ser compatíveis</h5>
                                                        <table class="table table-striped table-responsive table-hover">
                                                          <thead>				
                                                                <tr>
                                                                    <th class="col-xs-3">Código da Disciplina</th>
                                                                    <th class="col-xs-7">Nome da Disciplina</th>
                                                                    <th class="col-xs-2"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>2546786</td>
                                                                    <td>Teoremas de Pitágoras</td>
                                                                    <td><button class="btn btn-minier btn-primary"><i class="fa fa-plus"></i> Incluir</button></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2546786</td>
                                                                    <td>Teoremas de Pitágoras</td>
                                                                    <td><button class="btn btn-minier btn-primary"><i class="fa fa-plus"></i> Incluir</button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                        <!-------------------------- APARECER SE NÃO ENCONTRAR A DISCIPLINA ------------------------------>
                                                        
                                                        
                                                        
                                                        
                                                        <!-------------------------- APARECER SE NÃO ENCONTRAR A DISCIPLINA ------------------------------>
                                                        
                                                        <br /><br />
                                                        <div class="alert alert-danger">
                                                        	<strong><i class="fa fa-times" aria-hidden="true"></i> Nehuma disciplina encontrada no sistema!</strong> <br />Adicione manualmente utilizando o código ou o nome da disciplina:
                                                        </div>
                                                        
                                                        <form class="form-horizontal" method="get">
                                                          <div class="form-group">
                                                              <label class="control-label col-xs-12 col-sm-4 no-padding-right">Código da Disciplina:</label>
              
                                                              <div class="col-xs-12 col-sm-8">
                                                                  <input type="text" id="" name="" class="col-xs-12 col-sm-8 col-md-9 col-lg-9" />
                                                              </div>
                                                          </div>
                                                          
                                                          <div class="form-group">
                                                            <label class="control-label col-xs-12 col-sm-4 no-padding-right">Nome da Disciplina:</label>
            
                                                            <div class="col-xs-12 col-sm-8">
                                                                <input type="text" id="" name="" class="col-xs-12 col-sm-8 col-md-9 col-lg-9" />
                                                            </div>
                                                          </div>
                                                          
                                                          <button class="btn btn-primary btn-block"><i class="fa fa-plus" aria-hidden="true"></i> Incluir</button>
                                                      	</form>
                                                        
                                                        <!-------------------------- APARECER SE NÃO ENCONTRAR NO FORMULÁRIO ACIMA ------------------------------>
                                                        
                                                        
                                                        
                                                        
                                                        <br /><br />
                                                        <h5 class="blue pull-left">UNIVERSIDADE ESTÁCIO DE SÁ</h5>
                                                        <table class="table table-striped table-responsive table-hover">
                                                          <thead>				
                                                                <tr>
                                                                    <th class="col-xs-3">Código da Disciplina</th>
                                                                    <th class="col-xs-7">Nome da Disciplina</th>
                                                                    <th class="col-xs-2"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>2546786</td>
                                                                    <td>Teoremas de Pitágoras</td>
                                                                    <td><button class="btn btn-minier btn-danger"><i class="fa fa-trash"></i> Excluir</button></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2546786</td>
                                                                    <td>Teoremas de Pitágoras</td>
                                                                    <td><button class="btn btn-minier btn-danger"><i class="fa fa-trash"></i> Excluir</button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                        <br />
                                                        <h5 class="blue pull-left">UNIVERSIDADE DO ESTADO DO RIO DE JANEIRO</h5>
                                                        <table class="table table-striped table-responsive table-hover">
                                                          <thead>				
                                                                <tr>
                                                                    <th class="col-xs-3">Código da Disciplina</th>
                                                                    <th class="col-xs-7">Nome da Disciplina</th>
                                                                    <th class="col-xs-2"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>2546786</td>
                                                                    <td>Teoremas de Pitágoras</td>
                                                                    <td><button class="btn btn-minier btn-danger"><i class="fa fa-trash"></i> Excluir</button></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2546786</td>
                                                                    <td>Teoremas de Pitágoras</td>
                                                                    <td><button class="btn btn-minier btn-danger"><i class="fa fa-trash"></i> Excluir</button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                  
                                                </div>
                                            </div>

                                            <hr />
                                            <div class="row-fluid wizard-actions">
                                                <button class="btn btn-danger btn-prev">
                                                    <i class="fa fa-arrow-left"></i>
                                                    Anterior
                                                </button>
                                                
                                                <button class="btn btn-primary btn-next" data-last="Concluir ">
                                                    Próxima
                                                    <i class="fa fa-arrow-right"></i>
                                                </button>
                                            </div>
                                        </div><!-- /widget-main -->
                                    </div><!-- /widget-body -->



                                <!------------------------------------------------- FINAL - INSTITUICAO --------------------------------------------->
                            </div><!-- /widget-main -->
                        </div><!-- /widget-body -->
                    </div><!-- /forgot-box -->


                    <div class="row">
                        <div class="col-xs-12 login-info">
                            <hr />
                            <p><strong>Central de informações:</strong><br /> 21 3882-9797 | 21 3882-9752 | 21 98121-8095<br />
                                © 2016 UNISUAM. Todos os direitos reservados.</p>
                        </div>
                    </div>

                </div><!-- /position-relative -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.main-container -->




<!-- basic scripts -->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>
<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>
<!-- inline scripts related to this page -->

<script src="assets/js/jquery.maskedinput.min.js"></script>

</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-wizard.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 11 Apr 2014 00:52:08 GMT -->
</html>