<?php require_once("header.php"); ?>

<section class="admissao-resultado-wrapper admissao-resultado__topo">
	<section class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="admissao-title">RESULTADO DO VEST</h1>
			</div>
		</div>
		
		<!-- STATUS APROVADO CLASSIFICADO -->
		<div class="row admissao-status-wrapper">
				<div class="col-xs-12 col-md-12 text-center">
					<div class="box-classificado">
					  <h2><i class="fa fa-check" aria-hidden="true"></i> Classificado!</h3>
					  Seu próximo passo é preencher seus dados pessoais e comparecer à secretaria da sua Unidade UNISUAM com todos os documentos solicitados em mãos até o dia xx/xx/xxxx. O não comparecimento ou não entrega de todos os documentos solicitados dentro do prazo determinado, implica em perda do direito da bolsa.
					</div>
				</div>
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__aprovado">
					<i class="fa fa-thumbs-o-up fa-3x"></i>
					<h1 class="">APROVADO</H1>
				</div>
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
					<h2 class="admissao-status__title">Nota da Prova Objetiva</h2>
					<label class="aprovado-green-circle">
						100
					</label>
				</div>
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
					<h2 class="admissao-status__title">Nota da Redação</h2>
					<label class="aprovado-green-circle">
						81
					</label>
				</div>
		</div>

		<!-- STATUS APROVADO NÃO CLASSIFICADO -->
		<div class="row admissao-status-wrapper">
				<div class="col-xs-12 col-md-12 text-center">
					<div class="box-nao-classificado">
					  <h2><i class="fa fa-frown-o" aria-hidden="true"></i> Não Classificado.</h3>
					  Infelizmente você ainda não está classificado para efetuar sua matrícula com a bolsa solidária, mas fique atento às datas do edital e ao seu e-mail para saber das novidades, pois pode haver reclassificação.
					</div>
				</div>
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__aprovado">
					<i class="fa fa-thumbs-o-up fa-3x"></i>
					<h1 class="">APROVADO</H1>
				</div>
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
					<h2 class="admissao-status__title">Nota da Prova Objetiva</h2>
					<label class="aprovado-green-circle">
						78
					</label>
				</div>
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
					<h2 class="admissao-status__title">Nota da Redação</h2>
					<label class="aprovado-green-circle">
						75
					</label>
				</div>
		</div>
		
		<!-- STATUS REPROVADO -->
		<div class="row admissao-status-wrapper">
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__reprovado">
					<i class="fa fa-frown-o fa-3x"></i>
					<h1 class="">REPROVADO</H1>
				</div>
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
					<h2 class="admissao-status__title">Nota da Prova Objetiva</h2>
					<label class="reprovado-red-circle">
						0
					</label>
				</div>
				<div class="col-xs-12 col-md-4 admissao-status__aprovacao admissao-status__nota">
					<h2 class="admissao-status__title">Nota da Redação</h2>
					<label class="reprovado-red-circle">
						0
					</label>
				</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<button type="button" class="btn-imprimir-resultado center-block"><i class="fa fa-print"></i>  IMPRIMIR O RESULTADO</button>
			</div>
		</div>
	</section>
</section>
<section class="container">

	<!--

	<section class="admissao-resultado-wrapper admissao-resultado__pagamento">
		<p class="admissao-subtitle admissao-resultado__info"><strong>Sua escolha foi:</strong> <span class="laranja">Administração</span> | <span class="laranja">Bonsucesso</span> | <span class="laranja">Noite</span> | Preço fixo de R$<span class="laranja">660,00</span><small>/mês</small>
		<hr />
		<h1 class="admissao-title">FORMAS DE PAGAMENTO</h1>
		
		<div class="row admissao-pagamento-wrapper">
			<div class="col-xs-12 col-md-4">
				<h2 class="admissao-status__title">Parcelas Disponíveis:</h2>
			</div>
			<div class="col-xs-12 col-md-8 radio-parcelas">
				
					<label class="radio">
					 <input type="radio" name="optionsRadios"  id="parcelas-5x" value="5 vezes" checked>    <label for="parcelas-5x"><em>5x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</em></label>
					</label>
					
					<label class="radio">
					 <input type="radio" name="optionsRadios"  id="parcelas-6x" value="6 vezez">    <label for="parcelas-6x"><em>6x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</em></label>
					</label>
					
					<label class="radio">
					 <input type="radio" name="optionsRadios"  id="parcelas-7x" value="7 vezes">    <label for="parcelas-7x"><em>7x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</em></label>
					</label>
					
					<label class="radio">
					 <input type="radio" name="optionsRadios"  id="parcelas-8x" value="8 vezes">    <label for="parcelas-8x"><em>8x de R$ 550,00 - vencimento da 1ª parcela: 10/11/2017</em></label>
					</label>
					
			</div>
		</div>	
			
			<div class="row">
				<div class="col-xs-12 admissao-contrato-wrapper">
					<h1 class="admissao-title">CONTRATO DE MATRÍCULA</h1>
					<div class="alert alert-warning alert__contrato">
						<i class="fa fa-warning"></i>
						<strong>Leia o contrato</strong> a seguir com atenção. Após, preencha os campos no final do texto para assinar virtualmente.
						<br>
					</div>
					
					<iframe class="iframe__contrato" src="contrato-2018-2.content.php"></iframe>
					<hr />
					<h1 class="admissao-title">ACEITE DO CONTRATO DE MATRÍCULA</h1>
				</div>
				
				<div class="col-xs-12 col-md-4 col-md-offset-4 admissao-assinatura-wrapper">
				
					<div class="input-group admissao-input">
						<label class="admissao-form__label">CPF:</label><br />
						<input type="text" class="form-control " placeholder="CPF" />
					</div>
					<div class="input-group admissao-input">
						<label class="admissao-form__label">Senha:</label><br />
						<input type="password" class="form-control " placeholder="Senha" />
					</div>
					<button class="admissao-submit-btn">ASSINAR</button>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs12 admissao-contrato-wrapper">
					<p class="assinatura-texto__p">Contrato assinado por <strong>Maurício Gonçalves Fernandes Pacheco</strong> no dia <strong>00/00/0000</strong> às <strong>00h00</strong>.</p>
					<br />
					<button type="button" class="btn-imprimir-resultado center-block"><i class="fa fa-print"></i>  IMPRIMIR CONTRATO</button>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-md-4 col-md-offset-4 admissao-conclusao-wrapper">
					<h1 class="admissao-title">MAIS UMA FASE CONCLUÍDA!</h1>
					<p class="conclusao-texto__p"><i class="fa fa-smile-o fa-3x"></i></p>
					<p class="conclusao-texto__p"><strong class="laranja">Parabéns!</strong> Você concluiu a sua inscrição.</p>
					<p class="conclusao-texto__p">Agora você está muito perto de #SERUNISUAM. Você será <strong>direcionado para seu Ambiente do Aluno</strong>, onde poderá <strong>enviar os documentos</strong> para concluir a sua matrícula.</p>
					<p class="conclusao-texto__p"><em>Ah! Vamos te enviar um e-mail com seu número de matrícula.</em></p>
					<button class="admissao-submit-btn">IR PARA O AMBIENTE</button>
				</div>
			</div>
			
		
	</section>

</section>	
				
		
			</div>
		</div>
	</section>



-->


	<!-----------------MODAL---------------------->
						<div id="upload-doc" class="modal" tabindex="-1">
						  <div class="modal-dialog modal-mobile">
							  <div class="modal-content">
								  <div class="modal-header modal-header-mobile">
									  <button type="button" class="close" data-dismiss="modal">&times;</button>
									  <h4 class="blue bigger">CPF</h4>
								  </div>

									<div class="modal-body overflow-visible">
										<div class="row margin-mobile-none">
											<div class="col-xs-12 col-sm-12 margin-mobile-none">
												
												<h5>Clique abaixo para procurar o arquivo ou arraste-o para o trecho destacado.</h5>
												
												<div class="widget-body">
													<div class="widget-main">
														<input multiple="" type="file" id="id-input-file-3" />														
													</div>
												</div>
												
												<br />
												
												<h4>Arquivos enviados</h4>
												<table id="sample-table-1" class="table table-striped table-hover table-responsive space-top">
													<thead>
														<tr>
															<th>Nome do arquivo</th>
															<th>Data de envio</th>
															<th>Excluir</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>cpf-mauricio-pacheco.pdf</td>
															<td>00/00/0000 às 00h00 </td>
															<td><button class="btn btn-xs btn-danger" data-toggle="modal"  href="#upload-doc"><i class="fa fa-trash bigger-120" aria-hidden="true"></i></button></td>
														</tr>
													</tbody>
												</table>
										  
											</div>
										</div>
									</div>

								  <div class="modal-footer modal-footer-mobile">
									  <button type="submit" class="btn btn-sm btn-primary">
										  <i class="fa fa-check"></i> 
										  Enviar para análise
									  </button>
									  
									  <button type="submit" class="btn btn-sm btn-danger" data-dismiss="modal">
										  <i class="fa fa-times"></i> 
										  Fechar sem enviar
									  </button>  
								  
								  </div>
							  </div>
						  </div>
					  </div><!-- MODAL -->
</section>

<?php require_once("footer.php"); ?>