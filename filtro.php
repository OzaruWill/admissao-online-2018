<?php require_once("header.php"); ?>


<div class="area-filtro">


<section class="container">

<!-- CUT -->

<div class="row" id="cut">

	<div class="row">
		<div class="col-md-12 filtro-criterio">
			Quero escolher por: <button id="btn-cut-curso" class="btn-cut-enabled">CURSO</button> <button id="btn-cut-unidade" class="btn-cut-disabled">UNIDADE</button>
		</div>
	</div>

	<div class="col-md-3">

		<div class="campo">
			<span class="rotulo">Escolha o <strong>curso</strong></span><br>
			<input type="text" class="form-group" name="">
			<a href="#" id="btnCurso"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
		</div><!-- campo -->
		
	</div><!-- col -->

	<div class="col-md-3">

		<div class="campo">
			<span class="rotulo">Escolha a <strong>unidade</strong></span><br>
			<input type="text" class="form-group" name="">
			<a href="#" id="btnUnidade"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
		</div><!-- campo -->
		
	</div><!-- col -->

	<div class="col-md-3">

		<div class="campo">
			<span class="rotulo">Escolha o <strong>turno</strong></span><br>
			<input type="text" class="form-group" name="">
			<a href="#" id="btnTurno"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
		</div><!-- campo -->
		
	</div><!-- col -->

	<div class="col-md-3">

			<button class="btn-filtrar">ESCOLHER</button>
		
	</div><!-- col -->

</div><!-- row -->

<!-- / CUT -->


<!-- CURSOS -->


<div id="cursos">

	<a href="#" id="btnFecharCursos">
		<i class="fa fa-times fa-2x fechar" aria-hidden="true"></i>
	</a>

<div class="row">

	<h3>ESCOLHA O SEU CURSO</h3>
	<div class="linha"></div>

</div><!-- row -->

<div class="row lista">

	<div class="col-md-3">

		<ul class="lista">
			<li><a href="#">Administração</a></li>
			<li><a href="#">Análise e Desenv. de Sistemas</a></li>
			<li><a href="#">Arquitetura e Urbanismo</a></li>
			<li><a href="#">Biologia (Bacharelado)</a></li>
			<li><a href="#">Ciência da Computação</a></li>
			<li><a href="#">Ciências Biológicas (Lic)</a></li>
			<li><a href="#">Ciências Contábeis</a></li>
			<li><a href="#">Direito</a></li>
		</ul>

	</div><!-- col -->

	<div class="col-md-3">

		<ul>
			<li><a href="#">Educação Física (Bac)</a></li>
			<li><a href="#">Educação Física (Lic)</a></li>
			<li><a href="#">Enfermagem</a></li>
			<li><a href="#">Engenharia Civil</a></li>
			<li><a href="#">Engenharia de Produção</a></li>
			<li><a href="#">Engenharia Elétrica</a></li>
			<li><a href="#">Engenharia Mecânica</a></li>
			<li><a href="#">Estética e Cosmética</a></li>
		</ul>

	</div><!-- col -->

	<div class="col-md-3">

		<ul>
			<li><a href="#">Farmácia</a></li>
			<li><a href="#">Fisioterapia</a></li>
			<li><a href="#">Formação Pedagógica</a></li>
			<li><a href="#">Gastronomia</a></li>
			<li><a href="#">Gestão de Recursos Humanos</a></li>
			<li><a href="#">História</a></li>
			<li><a href="#">Jornalismo</a></li>
			<li><a href="#">Logística</a></li>
		</ul>

	</div><!-- col -->

	<div class="col-md-3">

		<ul>
			<li><a href="#">Marketing</a></li>
			<li><a href="#">Nutrição</a></li>
			<li><a href="#">Pedagogia</a></li>
			<li><a href="#">Processos Gerenciais</a></li>
			<li><a href="#">Psicologia</a></li>
			<li><a href="#">Publicidade</a></li>
			<li><a href="#">Serviço Social</a></li>
			<li><a href="#">Turismo (Bacharelado)</a></li>
		</ul>

	</div><!-- col -->

</div><!-- row lista -->

</div><!-- cursos -->


<!-- / CURSOS -->



<!-- UNIDADES -->

<div id="unidades">

	<a href="#" id="btnFecharUnidades">
		<i class="fa fa-times fa-2x fechar" aria-hidden="true"></i>
	</a>

<div class="row">

	<h3>ESCOLHA O SEU CURSO</h3>
	<div class="linha"></div>

</div><!-- row -->

<div class="row lista">

	<div class="col-md-12 text-center">

		<ul>
			<li><a href="#">Bonsucesso</a></li>
			<li><a href="#">Campo Grande</a></li>
			<li><a href="#">Bangu</a></li>
			<li><a href="#">Jacarepaguá</a></li>
		</ul>

	</div><!-- col -->


</div><!-- row lista -->

</div><!-- unidades -->

<!-- / UNIDADES -->




<!-- TURNOS -->

<div id="turnos">

	<a href="#" id="btnFecharTurnos">
		<i class="fa fa-times fa-2x fechar" aria-hidden="true"></i>
	</a>

<div class="row">

	<h3>ESCOLHA O MELHOR TURNO</h3>
	<div class="linha"></div>

</div><!-- row -->

<div class="row lista">

	<div class="col-md-12 text-center">

		<ul>
			<li><a href="#">Manhã</a></li>
			<li><a href="#">Tarde</a></li>
			<li><a href="#">Noite</a></li>
		</ul>

	</div><!-- col -->

</div><!-- row lista -->

</div><!-- unidades -->

<!-- / TURNOS -->

</section><!--/container-->
	
</div><!-- area-filtro -->


<div class="bg-cinza">

<section class="container">

<div class="row" style="margin: 20px 0 20px 0;">
	<h1 class="admissao-form-title">ADMINISTRAÇÃO</h1>
</div><!-- row -->

<div class="row curso-text">

	<div class="col-md-5">


		<div class="curso-titulo-cinza">Sua escolha:</div>

		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 20px;">
		  <tr>
		    <td width="30%" class="curso-texto-light">Unidade</td>
		    <td width="30%" class="curso-texto-laranja"><strong>Bonsucesso</strong></td>
		    <td width="40%">
		    	<button class="btn-trocar"><i class="fa fa-exchange" aria-hidden="true"></i> Trocar</button>
		    </td>
		  </tr>
		  <tr>
		    <td class="curso-texto-light">Turno</td>
		    <td class="curso-texto-laranja"><strong>Noite</strong></td>
		    <td>
		    	<button class="btn-trocar"><i class="fa fa-exchange" aria-hidden="true"></i> Trocar</button>
		    </td>
		  </tr>
		</table>

		<div class="admissao-input" style="margin-bottom: 0;">

			<label class="curso-titulo-cinza">Mensalidade:</label>

			<label class="radio-inline">
			 <input type="radio" name="optionsRadios"  id="lingua-ingles" value="Inglês" checked>    <label for="lingua-ingles" style="font-weight: normal;">Presencial</label>
			</label>
			<label class="radio-inline">
			 <input type="radio" name="optionsRadios"  id="lingua-espanhol" value="Espanhol" checked>    <label for="lingua-espanhol" style="font-weight: normal;">A distância</label>
			</label>	
		</div>

		<span class="curso-texto-old">De 1.088,75/mês</span>Por<span class="curso-texto-laranja"> <strong>660,00</strong></span><span style="font-weight: normal;">/mês.</span>

		<div>
			<button type="submit" class="btn btn-default admissao-submit-btn" style="margin-bottom: 5px;">INSCREVA-SE</button>
		</div>

		<div class="obs-texto">* Estimativa para 2018 sem a promoção, considerando cerca de 8% de reajuste anual.</div>
			
	</div><!-- col -->

	<div class="col-md-3">

		<div class="curso-info">
			<i class="fa fa-user-o fa-2x" aria-hidden="true"></i>
			<h5>Coordenação:</h5>
			Prof. Josué José da Silva
		</div>

		<div class="curso-info">
			<i class="fa fa-calendar fa-2x" aria-hidden="true"></i>
			<h5>Duração Estimada:</h5>
			4 anos (8 períodos)
		</div>

	</div><!-- col -->

	<div class="col-md-4">

		  <ul class="nav nav-tabs nav-justified">
		  	<li id="estilo-disciplinas"><a href="#" id="btn-disciplinas">DISCIPLINAS</a></li>
		    <li id="estilo-corpo-docente" class="active"><a href="#" id="btn-corpo-docente">CORPO DOCENTE</a></li>
		  </ul>
		  <br>

		  <div class="content-tabela">

		  <table class="table table-striped texto-tabela no-border" id="tab-corpo-docente">
		    <thead>
		      <tr class="titulo-tabela">
		        <th>Professor</th>
		        <th>Titulação</th>
		        <th class="text-center">Lattes</th>
		      </tr>
		    </thead>

		    <tbody>
		      <tr>
		        <td>Alan Vieira de Almeida</td>
		        <td>Mestrado</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		      <tr>
		        <td>Alessandro Marinho Pinheiro</td>
		        <td>Mestrado</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		      <tr>
		        <td>Alexandre Carreira de Souza</td>
		        <td>Especialização</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		      <tr>
		        <td>Alexandre de Almeida Lima</td>
		        <td>Mestrado</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		      <tr>
		        <td>André Luiz Marques Gomes</td>
		        <td>Mestrado</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		      <tr>
		        <td>Augusto Cesar Dias</td>
		        <td>Mestrado</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		      <tr>
		        <td>Carlos Alexandre Duarte Correa</td>
		        <td>Mestrado</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		      <tr>
		        <td>Carlos Eduardo José da Silva</td>
		        <td>Mestrado</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		      <tr>
		        <td>Claudia Val dos Santos</td>
		        <td>Mestrado</td>
		        <td class="text-center"><a href="#"><i class="fa fa-file" aria-hidden="true"></i></a></td>
		      </tr>
		    </tbody>

		  </table>


		  <table class="table table-striped texto-tabela no-border" id="tab-disciplinas">
		    <thead>
		      <tr class="titulo-tabela">
		        <th>Disciplinas</th>
		      </tr>
		    </thead>

		    <tbody>
		      <tr>
		        <td>Metodologia do Trabalho Acadêmico e Científico</td>
		      </tr>
		      <tr>
		        <td>Contabilidade Básica</td>
		      </tr>
		      <tr>
		        <td>Fundamentos da Gestão</td>
		      </tr>
		      <tr>
		        <td>Matemática Aplicada</td>
		      </tr>
		      <tr>
		        <td>Leitura e Produção de Textos</td>
		      </tr>
		    </tbody>

		  </table>


		  </div><!-- content-tabela -->
		  <div style="clear:both;"></div>

	</div><!-- col -->

</section><!--/container-->
	
</div><!-- row -->


</div><!-- bg-cinza -->

<?php require_once("footer.php");?>