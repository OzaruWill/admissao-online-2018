
$(document).ready(function(){

	$('#btnCurso').click(function(){
		$('#cut').css("display","none");
		$('#cursos').css("display","block");
	});

	$('#btnFecharCursos').click(function(){
		$('#cut').css("display","block");
		$('#cursos').css("display","none");
	});

	$('#btnUnidade').click(function(){
		$('#cut').css("display","none");
		$('#unidades').css("display","block");
	});

	$('#btnFecharUnidades').click(function(){
		$('#cut').css("display","block");
		$('#unidades').css("display","none");
	});

	$('#btnTurno').click(function(){
		$('#cut').css("display","none");
		$('#turnos').css("display","block");
	});

	$('#btnFecharTurnos').click(function(){
		$('#cut').css("display","block");
		$('#turnos').css("display","none");
	});

	$('#btn-disciplinas').click(function(){
		$('#tab-disciplinas').css("display","block");
		$('#tab-corpo-docente').css("display","none");
		$('#estilo-disciplinas').addClass('active');
		$('#estilo-corpo-docente').removeClass('active');
	});

	$('#btn-corpo-docente').click(function(){
		$('#tab-disciplinas').css("display","none");
		$('#tab-corpo-docente').css("display","block");
		$('#estilo-disciplinas').removeClass('active');
		$('#estilo-corpo-docente').addClass('active');
	});

	$('#btn-cut-curso').click(function(){
		$(this).addClass('btn-cut-enabled');
		$(this).removeClass('btn-cut-disabled');
		$('#btn-cut-unidade').addClass('btn-cut-disabled');
		$('#btn-cut-unidade').removeClass('btn-cut-enabled');
	});

	$('#btn-cut-unidade').click(function(){
		$(this).addClass('btn-cut-enabled');
		$(this).removeClass('btn-cut-disabled');
		$('#btn-cut-curso').addClass('btn-cut-disabled');
		$('#btn-cut-curso').removeClass('btn-cut-enabled');
	});

});